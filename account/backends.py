from mongoengine.django.auth import MongoEngineBackend
from mongoengine.django.auth import User


class PasswordlessAuthBackend(MongoEngineBackend):
    """
    Log in to app without providing a password.

    """
    def authenticate(self, username=None):
        try:
            return User.objects.get(username=username)
        except User.DoesNotExist:
            return None

    
    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None