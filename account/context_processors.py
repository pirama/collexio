import json

from models import CUser, Following
from collection.views import collection_list
from serializers import serialize_my_collections


def side_panel_data(request):
    json_result = {}
    if request.user.is_authenticated():
        profile = CUser.objects.get(username=request.user.username)
        data = {}
        data['my_public'] = collection_list(request, 'public', profile=profile, pagination=False)[:100]
        data['my_private'] = collection_list(request, 'private', profile=profile, pagination=False)[:100]
        data['my_collaborations'] = profile.get_collaborations(request.user)[:100]
        json_result = serialize_my_collections(data)
    return {'my_collections': json.dumps(json_result)}
