from django import forms


class ProfileForm(forms.Form):
    name = forms.CharField()
    image = forms.ImageField(required=False)
    email = forms.EmailField(required=False)
    description = forms.CharField(required=False)
