from account.models import CUser
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def handle(self, *args, **options):
        users = CUser.objects.all()
        for user in users:
            user.follow("53b1112ae7798909989dce60")
            user.follow("53ec8bdd96540a3411ffa39e")
            user.follow("53cd1041e7798979b3f1eb49")
            user.follow("54d4ca9865c35b058425f84b")
            print user.username
