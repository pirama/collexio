# -*- coding: utf-8 -*-
import logging
from django.conf import settings
from django.utils.timezone import now as datetime_now
from mongoengine.django.auth import User
from mongoengine import *
from libs.fields import ImageDict
from collaboration.account_mixins import CollaborationUserMixin
from collection.mixins import SubscriptionsAccountMixin
from serializers import get_random_featured
from mongoengine import signals
from search.recievers import user_to_es, user_remove_es


class CUser(User, CollaborationUserMixin, SubscriptionsAccountMixin):
    name = StringField(max_length=30)
    description = StringField(max_length=160)
    email = StringField(max_length=160)
    image = ImageDict(thumb_tuple=(
        [960, 960, "large"],
        [640, 640, "medium"],
        [320, 320, "small"],
        [160, 160, "thumb"]
    ), required=False)
    is_featured = BooleanField(default=False)

    @property
    def f_name(self):
        name_str = self.name
        name_array = name_str.split(' ')
        return name_array[0]

    @property
    def l_name(self):
        name_str = self.name
        name_array = name_str.split(' ')
        if len(name_array) == 1:
            return ""
        else:
            return name_array[-1]

    @property
    def thumb_image(self):
        return settings.CDN_URL + self.image['medium']['filename']

    @classmethod
    def create_user(cls, username, name, description, image):
        """Create (and save) a new user with the given username, description and
        name.
        """
        now = datetime_now()
        # Normalize the address by lowercasing the domain part of the email
        # address.
        user = cls(username=username, name=name, description=description, image=image, date_joined=now)
        user.save()
        return user

    def update_featured(self):
        if self.is_featured:
            self.is_featured = False
        else:
            self.is_featured = True
        self.save()
        return self.is_featured


    def follow(self, following_id):
        try:
            following = CUser.objects.get(pk=following_id)
        except Exception:
            raise ObjectDoesNotExist("I can't find the record for who you want to follow.")

        try:
            if Following.objects.filter(user=self, following=following).count() == 0:
                follow_rec = Following(
                    user=self,
                    following=following
                )

                follow_rec.save()
            else:
                following = False
        except Exception:
            raise DatabaseError("I can't save your following record.")

        return following

    def unfollow(self, following_id):
        try:
            following = CUser.objects.get(pk=following_id)
        except Exception:
            raise ObjectDoesNotExist("I can't find the record for who you want to follow.")

        if Following.objects.filter(user=self, following=following).count() > 0:
            rec = Following.objects.filter(user=self, following=following)
            rec.delete()
        else:
            following = False
        return following

    def is_following(self, following):
        c = Following.objects.filter(user=self, following=following).count()
        if c > 0:
            return True
        else:
            return False

    @property
    def followers_count(self):
        c = Following.objects.filter(following=self).count()
        if not c:
            c = 0
        return c

    @property
    def following_count(self):
        c = Following.objects.filter(user=self).count()
        if not c:
            c = 0
        return c

    @property
    def followers(self):
        followers = Following.objects.filter(following=self)
        f_list = []
        for f in followers:
            f_list.append(f.user)
        return f_list

    @property
    def following(self):
        following = Following.objects.filter(user=self)
        f_list = []
        for f in following:
            f_list.append(f.following)
        return f_list

    @property
    def nt_count(self):
        from notifications.models import Notifications
        return Notifications.objects(to=self, _is_seen=False).count()

    @property
    def my_collection_count(self):
        from collection.models import Collection
        return Collection.objects.filter(owner=self).count()

    @property
    def collection_count(self):
        from collection.models import Collection
        return Collection.objects.filter(owner=self, is_private__ne=True).count()

    def random_featured(self):
        followed_pks = []
        followed = Following.objects.filter(user=self)
        for f in followed:
            followed_pks.append(str(f.following.pk))
        featured = CUser.objects.filter(is_featured=True, pk__ne=self.pk, pk__nin=followed_pks)[0:5]
        random_featured = get_random_featured(featured)
        return random_featured


    def send_mail(self, subject, tpl_name, context_dict={}):
        from django.core.mail import EmailMessage
        from django.template.loader import get_template
        from django.template import Context

        htmly = get_template('emails/'+tpl_name)
        context_dict['user'] = self
        d = Context(context_dict)
        html_content = htmly.render(d)

        msg = EmailMessage(subject, html_content, settings.EMAIL_FROM_ADDR, [self.email])
        msg.content_subtype = "html"  # Main content is now text/html
        msg.send()


class Following(Document):
    user = ReferenceField(CUser, reverse_delete_rule=CASCADE)
    following = ReferenceField(CUser, reverse_delete_rule=CASCADE)


# Signals for Elasticsearch update

signals.post_save.connect(user_to_es, sender=CUser)
signals.post_delete.connect(user_remove_es, sender=CUser)

