# -*- coding: utf-8 -*-
import logging
import json

from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse, Http404, HttpResponseBadRequest
from django.template import RequestContext
from django.core.paginator import Paginator
from mongoengine import Q
from collection.models import *
from collection.serialize import *
from notifications.models import *

from models import CUser, Following



def userlist_autocomplete(request):
    data = {}
    data['messages'] = {}

    username = request.GET.get('username', None)

    if len(username)>=3:
        followers = request.user.followers()
        f_list = []
        for f in followers:
            f_list.append(f.user.pk)

        users = CUser.objects.filter(Q(username__istartswith=username) | Q(name__icontains=username),pk__in=f_list)[0:5]

        userlist = []
        for user in users:
            userlist.append(serialize_user(user))

        data['results'] = userlist
        data['status'] = "OK"
        return HttpResponse(
            json.dumps(data),
            content_type='application/javascript; charset=utf8'
        )

    else:
        data['messages']['error'] = "Parameter error."
        data['results'] = []

    return HttpResponse(
        json.dumps(data),
        content_type='application/javascript; charset=utf8'
    )


def follow(request, following_id):
    dd = {}
    dd['messages'] = {}
    f_user = CUser.objects.get(pk=following_id)

    if request.method == 'GET':
        following = request.user.follow(following_id)
        if following:
            dd['messages']['success'] = "You started to following {0}".format(f_user.username)

            FollowedNotification(
                to=following,
                nt_from=request.user
            ).send_nt()
        else:
            return HttpResponseBadRequest("You're already following {0}".format(f_user.username))  # TODO: Check translation)

    return HttpResponse(
        json.dumps(dd),
        content_type='application/javascript; charset=utf8'
    )


def unfollow(request, following_id):
    dd = {}
    dd['messages'] = {}
    f_user = CUser.objects.get(pk=following_id)

    if request.method == 'GET':
        following = request.user.unfollow(following_id)
        if following:
            dd['messages']['success'] = "You are not following {0} any more".format(f_user.username)
        else:
            return HttpResponseBadRequest("You do not follow {0}".format(f_user.username))  # TODO: Check translation

    return HttpResponse(
        json.dumps(dd),
        content_type='application/javascript; charset=utf8'
    )


def get_notifications(request):
    dd = {}
    dd['messages'] = {}

    notifications = Notifications.objects(to=request.user).order_by('-created')

    p = Paginator(notifications, 10)
    r_page = request.GET.get('p', 1)
    nt_list = p.page(r_page)
    if nt_list.has_next():
        dd['next_page_number'] = nt_list.next_page_number()

    dd['notifications'] = []
    for nt in nt_list:
        try:
            dd['notifications'].append(serialize_notification(nt))
        except:
            pass

    return HttpResponse(
        json.dumps(dd),
        content_type='application/javascript; charset=utf8'
    )


