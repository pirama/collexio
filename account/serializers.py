from django.conf.urls import url, include
# from auth import *
from django.template import defaultfilters
from random import shuffle

import mongoengine
# from rest_framework import routers, serializers, viewsets


# Serializers define the API representation.
# class UserSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = CUser
#         fields = ('url', 'username', 'email', 'is_staff')


def serialize_my_collections(data):
    json_result = {}
    json_result['my_public'] = {}
    json_result['my_private'] = {}
    json_result['my_collaborations'] = {}
    for content_type in data:
        if data[content_type]:
            for item in data[content_type]:
                json_result[content_type][str(item.pk)] = {
                    'title': item.title,
                    'collexio_url': item.collexio_url,
                    'item_count': item.item_count,
                    'created': defaultfilters.date(item.created, "F d"),
                    'get_small_cover_thumb': item.get_small_cover_thumb if item.get_small_cover_thumb else False,
                    'owner': item.owner.username
                }
    return json_result


# TODO: Rewrite this ugly solution
def get_random_pks(list_of_things):
    pks = []
    if isinstance(list_of_things, mongoengine.queryset.queryset.QuerySet):
        for thing in list_of_things:
            pks.append(str(thing.pk))
        shuffle(pks)
        return pks
    else:
        raise ValueError("Invalid object Type")


def get_random_featured(featured):
    from account.models import CUser
    result = []
    for pk in get_random_pks(featured):
        result.append(CUser.objects.get(pk=pk))
    return result