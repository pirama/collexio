from django.conf.urls import patterns, url


urlpatterns = patterns(
    '',
    url(r'^logout/$', 'account.views.signout', name='signout'),
    url(r'^signin/$', 'account.views.twitter_signin', name='signin'),
    url(r'^signin_view/$', 'account.views.signin', name='signin_view'),
    url(r'^auth/$', 'account.views.auth', name='tw-callback'),
    url(r'^(?P<username>\w+)/delete/$', 'account.views.delete', name='delete-account'),
    url(r'^(?P<username>\w+)/settings/$', 'account.views.preferences', name='settings'),
)
