# -*- coding: utf-8 -*-
import logging
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.contrib.auth import login, logout, authenticate
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.generic import ListView
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.contrib import messages
from django.utils.http import urlencode
import tweepy
import mailchimp
from libs.mailchimp_utils import get_mailchimp_api

from models import CUser
from forms import *
from collection.models import *
from collection.views import *
from notifications.models import *



def signin(request):
    ref = request.GET.get('ref', None)

    if ref == "ext":
        signin_message = "Just signin before collecting items via Chrome Extension."
    ctx = {'signin_message': signin_message}
    return render_to_response('signin.html', ctx, context_instance=RequestContext(request))


@login_required(login_url=settings.LOGIN_URL)
def signout(request):
    logout(request)
    return HttpResponseRedirect(reverse('main'))


@login_required(login_url=settings.LOGIN_URL)
def preferences(request, username):

    if request.method == 'POST':
        form = ProfileForm(request.POST, request.FILES)

        if form.is_valid():
            logging.debug('form valid')
            user = request.user
            user.name = form.cleaned_data['name']
            user.email = form.cleaned_data['email']
            user.description = form.cleaned_data['description']

            try:
                user.save()
                messages.add_message(request, messages.SUCCESS, 'Your settings has been saved!')
            except Exception:
                messages.add_message(request, messages.ERROR, "Ups! There's a problem. I can't complete your request!")

            if 'image' in request.FILES:
                logging.debug('image found')
                if request.FILES['image']:
                    logging.debug('image found')
                    user.image = request.FILES['image']
                    try:
                        user.save()
                    except Exception:
                        messages.add_message(request, messages.ERROR, "Ups! There's a problem. I can't complete your request!")
            else:
                logging.debug('image not found')
        else:
            logging.debug('form invalid')
            messages.add_message(request, messages.ERROR, "Ups! There's a problem. I can't complete your request!")
    else:
        form = ProfileForm()

    context_dict = {'form': form}

    return render_to_response('settings.html', context_dict, context_instance=RequestContext(request))


@login_required(login_url=settings.LOGIN_URL)
def delete(request, username):
    user = CUser.objects.get(username=username)
    if user == request.user:
        user.delete()
        messages.add_message(request, messages.SUCCESS, "Your Collexio account has been deleted.")
        return HttpResponseRedirect(reverse('main'))
    else:
        return HttpResponseRedirect("/account/"+request.user+"/settings/")


def profile(request, username, tab='public'):
    profile = CUser.objects.get(username__iexact=username)

    if tab == 'private':
        if request.user == profile:
            c_list = collection_list(request, "private", profile=profile)
        else:
            tab = "public"

    elif tab == 'unlisted':
        if request.user == profile:
            c_list = collection_list(request, "unlisted", profile=profile)
        else:
            tab = "public"

    elif tab == 'collaborations':
        collections = profile.get_collaborations(request.user)
        p = Paginator(collections, 18)
        r_page = request.GET.get('p', 1)
        c_list = p.page(r_page)

    else:
        c_list = collection_list(request, "public", profile=profile)

    is_following = None
    if request.user.is_authenticated():
        is_following = str(request.user.is_following(profile))


    # profile.collection_count = Collection.objects.filter(owner=profile, is_private__ne=True).count()
    ctx = {'collections': c_list, 'tab': tab, 'profile': profile, 'is_following': is_following}
    return render_to_response('profile.html', ctx, context_instance=RequestContext(request))


def followers(request, username):
    profile = CUser.objects.get(username=username)

    is_following = None
    if request.user.is_authenticated():
        is_following = str(request.user.is_following(profile))
    #profile.collection_count = Collection.objects.filter(owner=profile).count()

    profile_list = []
    for f in profile.followers:
        if request.user.is_authenticated():
            f.is_following_by_current_user = str(request.user.is_following(f))
        else:
            f.is_following_by_current_user = None

        profile_list.append(f)

    ctx = {'profile_list': profile_list, 'profile': profile, 'is_following': is_following, 'title': "FOLLOWERS"}
    return render_to_response('profile.html', ctx, context_instance=RequestContext(request))


def following_users(request, username):
    profile = CUser.objects.get(username=username)

    is_following = None
    if request.user.is_authenticated():
        is_following = str(request.user.is_following(profile))
    #profile.collection_count = Collection.objects.filter(owner=profile).count()

    profile_list = []
    for f in profile.following:
        if request.user.is_authenticated():
            f.is_following_by_current_user = str(request.user.is_following(f))
        else:
            f.is_following_by_current_user = None

        profile_list.append(f)

    ctx = {'profile_list': profile_list, 'profile': profile, 'is_following': is_following, 'title': "FOLLOWING"}
    return render_to_response('profile.html', ctx, context_instance=RequestContext(request))


@xframe_options_exempt
def twitter_signin(request):
    auth = tweepy.OAuthHandler(
        settings.TWITTER_CONSUMER_KEY, settings.TWITTER_CONSUMER_SECRET, settings.TWITTER_OAUTH_CALLBACK
    )
    auth.secure = True
    request.session['next'] = request.GET.get('next', '/')

    url = auth.get_authorization_url(signin_with_twitter=True)
    request.session['request_token'] = (auth.request_token.key, auth.request_token.secret)

    return HttpResponseRedirect(url)


@xframe_options_exempt
def twitter_authenticated(request):

    auth = tweepy.OAuthHandler(
        settings.TWITTER_CONSUMER_KEY, settings.TWITTER_CONSUMER_SECRET, settings.TWITTER_OAUTH_CALLBACK
    )

    auth.secure = True
    token = request.session['request_token']
    verifier = request.GET.get('oauth_verifier')
    auth.set_request_token(token[0], token[1])
    auth.get_access_token(verifier)

    api = tweepy.API(auth)
    auth_data = api.verify_credentials()

    #logging.debug(auth_data.__dict__)

    user = authenticate(username=auth_data.screen_name)
    if user is not None:
        if user.is_active:
            login(request, user)

    else:
        user = CUser.create_user(
            auth_data.screen_name,
            auth_data.name,
            auth_data.description,
            auth_data.profile_image_url.replace('_normal', '')
        )

        user.save()

        user = authenticate(username=auth_data.screen_name)
        if user is not None:
            if user.is_active:
                login(request, user)

    return HttpResponseRedirect(reverse('main'))


def auth(request):
    if request.method == 'POST':
        if 'email' in request.POST:
            auth_data = request.session['auth_data']
            user = CUser.create_user(
                auth_data['screen_name'],
                auth_data['name'],
                auth_data['description'],
                auth_data['profile_image_url'].replace('_normal', '')
            )

            user.email = request.POST['email']
            user.save()

            try:
                m = get_mailchimp_api()
                m.lists.subscribe(
                    "fb9e25041c",
                    {'email': user.email},
                    {'fname': user.f_name, 'lname': user.l_name},
                    double_optin=False
                )
                messages.success(request, "The email has been successfully subscribed")
                logging.debug("The email has been successfully subscribed")
            except mailchimp.ListAlreadySubscribedError:
                messages.error(request,  "That email is already subscribed to the list")
                logging.debug("That email is already subscribed to the list")
            except mailchimp.Error, e:
                messages.error(request,  'An error occurred: %s - %s' % (e.__class__, e))
                logging.debug('An error occurred: %s - %s' % (e.__class__, e))

            user = authenticate(username=auth_data['screen_name'])
            if user is not None:
                if user.is_active:
                    login(request, user)

                    NewUserNotification(
                        nt_from=request.user,
                    ).send_nt()

                    user.send_mail('Welcome to Collexio', 'welcome.html')

                    if 'next' in request.session:
                        next_page = request.session['next']
                    else:
                        next_page = '/'

                    return HttpResponseRedirect(next_page+"?signup=true")

            return HttpResponseRedirect(reverse('main'))

    auth = tweepy.OAuthHandler(
        settings.TWITTER_CONSUMER_KEY, settings.TWITTER_CONSUMER_SECRET, settings.TWITTER_OAUTH_CALLBACK
    )

    auth.secure = True
    token = request.session['request_token']
    verifier = request.GET.get('oauth_verifier')
    auth.set_request_token(token[0], token[1])
    auth.get_access_token(verifier)

    api = tweepy.API(auth)
    auth_data = api.verify_credentials()

    user = authenticate(username=auth_data.screen_name)
    if user is not None:
        if user.is_active:
            login(request, user)

            SignInNotification(
                nt_from=request.user,
            ).send_nt()

            if 'next' in request.session:
                next_page = request.session['next']
            else:
                next_page = '/'

            return HttpResponseRedirect(next_page+"?signup=false")
    else:
        auth_data_dict = {
            'screen_name': auth_data.screen_name,
            'name': auth_data.name,
            'description': auth_data.description,
            'profile_image_url': auth_data.profile_image_url,
        }
        request.session['auth_data'] = auth_data_dict
        ctx = {}
        return render_to_response('get_email.html', ctx, context_instance=RequestContext(request))


def pages(request, template):
    ctx = {}
    return render_to_response(template+'.html', ctx, context_instance=RequestContext(request))

