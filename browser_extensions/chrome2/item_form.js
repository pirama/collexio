/*
Collection List For User
*/
$("#item-form").hide();
var preloader = $(".preloader");
preloader.show();
$('#remove-img-btn').hide();

$.ajax({
	type: "GET",
	url: "https://collex.io/r/a/user_collections/",  // or just url: "/my-url/path/"
	dataType: "json",
	contentType: "json",
	success: function(data) {
		console.log(data);

		if (data['messages']['error'] == "Not Authenticated") {
			chrome.tabs.create({url: 'https://collex.io/account/signin_view/?ref=ext'});
		}
		else {
			var c_list = data.c_list;
			$.each(c_list,function(id){
				var c=c_list[id];
				$('#collection-list')
					.append($("<option></option>").attr("data-slug",c.slug).attr("value",c.id).text(c.title));
			});

			getPageContent();
			console.log($('#add-item-form-url').val());
		}
	},
	error: function(xhr, textStatus, errorThrown) {
		console.log(errorThrown);
	}
});

/* 
Fetch Page Content
*/

var getPageContent = function(){
	$.ajax({
		type: "GET",
		url: "https://collex.io/c/get_site_content/",  // or just url: "/my-url/path/"
		dataType: "json",
		contentType: "json",
		data: {
			url: $('#add-item-form-url').val()
		},
		success: function(data) {
			console.log(data);
			if (data.preview.html){
				addCard(data);
			}else if (data.image!=undefined){
				addImage(data);
			}

			$('#add-item-source').val(data.source);
			$('#add-item-title').val(data.title);
			$('#add-item-description').val(data.description);
			$('#add-item-image-url').val(data.image);

			$("#item-form").show();
			preloader.hide();
		},
		error: function(xhr, textStatus, errorThrown) {
			console.log(errorThrown);
			$("#item-form").show();
			preloader.hide();
		}
	});
}

/*
$(function () {
	var progress = 0;
	if ($("#item-image").length) {
		console.log($("#item-image"));
	    $('#item-image').fileupload({
	        dataType: 'json',
	        add: function(e,data){
	        	$(".modal-messages").hide();
	        	var file = e.originalEvent.delegatedEvent.target.files[0];
	        	var file_type = file.type;
	        	var file_size = file.size;
	        	if (file_type != "image/jpeg" && file_type != "image/png" && file_type != "image/gif"){
	        		$(".modal-messages").show();
	        		$(".modal-messages > span").html("Invalid file type. Jpeg, PNG and Gif files accepted.");
	        		return false;
	        	}
	        	if (file_size >= 5242880){
	        		$(".modal-messages").show();
	        		$(".modal-messages > span").html("Too big file. File size must be smaller than 5MB.");
	        		return false;
	        	}
	        	if (data.autoUpload || (data.autoUpload !== false &&
			            $(this).fileupload('option', 'autoUpload'))) {
			        data.process().done(function () {
			            data.submit();
			        });
			    }

	        },
	        done: function (e, data) {
	        	var image_el = '<img src="/'+data.result.file_path+'" class="add-item-image" />';
	            $('.item-image-preview').html(image_el);
	            $('#add-item-image-url').val(data.result.file_path);
	            $('#media-removed').val('False');
	        },
	        change: function (e, data) {
		    	progress = 0;
		    	$('#item-image-progress .bar').css(
		            'width',
		            progress + '%'
		        );  
		    },
	        progressall: function (e, data) {
		        progress = parseInt(data.loaded / data.total * 100, 10);
		        $('#item-image-progress .bar').css(
		            'width',
		            progress + '%'
		        );
		    }
	    });
	}
});
*/

$(".delete-item").on("click", function() {	
	var itemId = $(this).attr("data-item-id");
	var href = "/c/i/"+itemId+"/delete/";
	$("#deleteItemBtn").attr("href", href);
});

$("#add-item-form-save-btn").on('click',function(){
	save();
});

$('#add-item-form-open-collection-btn').on('click',function(){
	save(function(){
		var slug = $( "select option:selected" ).attr('data-slug');
		chrome.tabs.create({url: 'http://collex.io/c/'+slug+"/"});
	});
});

$('.item-form-close').on('click',function(){
	window.close();
});

var save = function(callback){
	preloader.show();
	$("#add-item-form").hide();
	var url = "https://collex.io/r/c/"+$('#collection-list').val()+"/add/";
	var isExtension = true;
	$.ajax({
		type: "POST",
		url: url,
		dataType: "json",
		data: $("#item-form").serialize(),
		success: function(data) {
			console.log(data);
			$(".spinner-icon").hide();

	        if (data.messages.error){
	        	$(".message").show();
	        	$(".message > span").html(data.messages.debug);
	        	$("#item-form").show();
				preloader.hide();
	        }
			else {
				preloader.hide();
				$('.message').show();
				$('.message').html("Your item has been saved.");
				/*
				setTimeout(function(){
					window.close();
				},2000);
				*/
			}
		},
		error: function(jqXHR,textStatus,errorThrown) {
			console.log(textStatus);
			$("#item-form").show();
			preloader.hide();
			$('.message').hide();
		},
		complete: function(){
			if (callback){
				callback();	
			}
		}
	});
	return false;
}

var addImage = function(data){
	$('.item-image-preview').html('<img src="'+data.image+'" class="add-item-image"/>');
	$('#remove-img-btn').show();
	$('.card-preview').hide();
}

var editImage = function(data){
	$('.item-image-preview').html('<img src="'+data.image+'" class="add-item-image"/>');
	$('#remove-img-btn').show();
	$('.card-preview').hide();
}

var addCard = function(data){
	$('.card-preview').html(data.preview.html);
	$('.card-preview').fitVids({ customSelector: "iframe[src^='//vine.co'], iframe[src^='http://myviiids.com']"});
	$('.embedly-url').embedly({key: '1457678e02504979b6c7eb1170968ddd'});
	$('#card-data').val(JSON.stringify(data.card));
	$('.card-preview').show();
	$('.image-frame').hide();
}

var resetItemForm = function(){
	$("#item-form").show();
	$("#url-form").show();
	$("#add-item-form").get(0).reset();
	$('#add-item-source').val(null);
	$('#add-item-image-url').val(null);
	$('.item-image-preview').html("");
	$('.image-frame').show();
	$('#card-data').val(null);
	$('#add-item-title').val(null);
	$('#item-form-loading').hide();
	$(".modal-messages").hide();
	$('.card-preview').hide();
}

$('#add-item-form-reset-btn').on('click',function(){
	window.close();
});	

$("#addItemModal").on('hidden.bs.modal',function(){
	resetItemForm();
});

$('#remove-img-btn').on('click',function(){
	$('#add-item-image-url').val(null);
	$('.item-image-preview').html("");
	$('.image-frame').show();
	$('#media-removed').val('True');
	$('#remove-img-btn').hide();
});

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
