from mongoengine import Q

class CollaborationUserMixin(object):
    def get_collaborations(self, user):
        from collection.models import Collection
        return Collection.objects(Q(is_private__ne=True) | Q(is_unlisted__ne=True) | Q(collaborators__user=user),
                                  collaborators__user=self)
