from django import forms
from account.models import CUser
from collection.models import Collection
from models import Collaborator

class CollaboratorForm(forms.Form):
    collection_id = forms.CharField(required=True)
    user_id = forms.CharField(required=True)
    email = forms.EmailField(required=False)
    is_accepted = forms.BooleanField(required=False, initial=False)
    permission = forms.CharField(required=True)

    def __init__(self, *args, **kwargs):
        self.instance = kwargs.pop('instance', None)
        super(CollaboratorForm, self).__init__(*args, **kwargs)

    def save(self):
        c = self.instance if self.instance else Collaborator()
        c.user = CUser.objects.get(pk=self.cleaned_data['user_id'])
        if self.cleaned_data['email']:
            c.email = self.cleaned_data['email']
        c.permissions = self.cleaned_data['permission']

        try:
            collection = Collection.objects.get(pk=self.cleaned_data['collection_id'])
        except Exception, e:
            raise e

        collection.add_collaborator(c)

        return c