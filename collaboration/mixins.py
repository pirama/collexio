import logging
from mongoengine import *
from models import Collaborator

class CollaborationMixin(object):
    collaborators = ListField(EmbeddedDocumentField(Collaborator))

    def get_collaborators(self):
        return self.collaborators

    def get_collaborator(self, user):
        for c in self.collaborators:
            if c.user == user:
                return c
        return False


    def add_collaborator(self, collaborator):
        from account.models import CUser

        user = CUser.objects.get(pk=collaborator.user.id)
        self.update(pull__collaborators__user=user)
        self.reload()
        self.collaborators.append(collaborator)
        self.save()

    def delete_collaborator(self, collaborator_id):
        from account.models import CUser
        user = CUser.objects.get(pk=collaborator_id)
        self.update(pull__collaborators__user=user)

    
    def check_permission(self, user, permission):
        if user.is_superuser and not self.is_private:
            return True

        if self.owner == user:
            return True

        if permission == "canview" and not self.is_private:
            return True

        perm = self.get_collaborator(user)
        if not perm:    
            return False

        if perm.permissions == permission:
            return True

        elif perm.permissions == "admin":
            return True

        elif permission == "canview" and perm.permissions == "canadd":
            return True

        else:
            return False

    def get_permission(self, user):
        if self.owner == user:
            return "admin"

        perm = self.get_collaborator(user)
        if perm:
            return perm.permissions
        else:
            return False
