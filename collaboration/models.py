from mongoengine import *
#from notifications.models import Notifications

class Collaborator(EmbeddedDocument):
    from account.models import CUser

    user = ReferenceField(CUser, required=False)
    email = EmailField(required=False)
    status = StringField(default="pending")
    is_accepted = BooleanField(default=False)
    permissions = StringField(default="canview")

    def clean(self):
        if self.user is None and self.email is None:
            raise ValidationError("You have to provide a username or email")