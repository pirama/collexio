import json
from account.models import CUser
from collection.models import Collection
from collection.serialize import *
from collexio.utils.rest import json_response, test_response
from collaboration.forms import CollaboratorForm
from notifications.models import CollaborationNotification

def serialize_collaborators(collaborator):
    if not isinstance(collaborator, list):
        c = collaborator
        return [{
            'user': serialize_user(c.user),
            'status': c.status,
            'is_accepted': c.is_accepted,
            'permission': c.permissions
        }]

    c_list = []
    for c in collaborator:
        c_list.append({
            'user': serialize_user(c.user),
            'status': c.status,
            'is_accepted': c.is_accepted,
            'permission': c.permissions
        })
    return c_list

def collaborators(request, collection_id, collaborator_id=None, action=None):
    data = {}
    data['messages'] = {}
    try:
        collection = Collection.objects.get(pk=collection_id)
    except Exception:
        data['messages']['error'] = "Collection not found."
        return json_response(data)

    if request.user != collection.owner:
        data['messages']['error'] = "Permission Error."
        return json_response(data)

    #DELETE
    if action == "delete":
        collection.delete_collaborator(collaborator_id)
        data['messages']['success'] = "Collaborator has been deleted."
        data['status'] = "ok"
        return json_response(data)

    #GET
    elif request.method == "GET":
        collaborators = collection.get_collaborators()
        data['result'] = serialize_collaborators(collaborators)
        data['status'] = "ok"
        return json_response(data)

    #POST
    elif request.method == "POST":
        form = CollaboratorForm(data=json.loads(request.body))
        if form.is_valid():
            c = form.save()

            CollaborationNotification(
                to=c.user,
                nt_from=request.user,
                collection=collection
            ).send_nt()

            data['result'] = serialize_collaborators(c)
            data['messages']['success'] = "Collaborator has been added."
            data['status'] = "ok"

        else:
            data['messages']['error'] = "Invalid Data"
            data['messages']['errors'] = form.errors
            data['status'] = "fail"

        return json_response(data)
