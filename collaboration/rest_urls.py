from django.conf.urls import patterns, include, url
from django.conf import settings

# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^(?P<collection_id>\w+)/collaborators$', 'collaboration.rest.collaborators'),
	url(r'^(?P<collection_id>\w+)/collaborators/(?P<collaborator_id>\w+)/(?P<action>\w+)$', 'collaboration.rest.collaborators'),
)