from django.conf.urls import patterns, url, include

urlpatterns = patterns(
    '',
	url(r'^(?P<collection_id>[-\w]+)/contributions/$', 'collaboration.views.collaboration_settings', name='collection-collaboration-settings'),
)