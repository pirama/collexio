from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.conf import settings
from collection.models import Collection


@login_required(login_url=settings.LOGIN_URL)
def collaboration_settings(request, collection_id):
    collection = Collection.objects.get(pk=collection_id)
    ctx = {'collection': collection}
    return render_to_response('collaboration_settings.html', ctx, context_instance=RequestContext(request))