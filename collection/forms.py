from django.core.validators import URLValidator
from django import forms
from models import *
import logging
from bson import ObjectId


class ItemForm(forms.Form):
    url = forms.CharField(required=False)
    title = forms.CharField(required=False)
    image_url = forms.CharField(required=False)
    card_data = forms.CharField(required=False)
    description = forms.CharField(required=False)
    source = forms.CharField(required=False)
    suggested = forms.CharField(required=False, initial=False)

    def clean(self):
        cleaned_data = super(ItemForm, self).clean()

        logging.debug(cleaned_data)
        if (self.cleaned_data['url'] is None or self.cleaned_data['url'] is u'') and \
                (self.cleaned_data['title'] is None or self.cleaned_data['title'] is u'') and \
                (self.cleaned_data['description'] is None or self.cleaned_data['description'] is u'') and \
                (self.cleaned_data['image_url'] is None or self.cleaned_data['image_url'] is u'' or self.cleaned_data['image_url'] is 'None') and \
                (self.cleaned_data['card_data'] is u'' or self.cleaned_data['card_data'] is 'None'):
            raise forms.ValidationError("Please provide at least one field.")

        return cleaned_data


STYLE_CHOICES = ('list', 'magazine', 'gallery', 'pinboard')
SORT_CHOICES = ('firsttolast', 'lasttofirst')
VISIBILITY_CHOICES = ('public', 'unlisted', 'private')


class CollectionForm(forms.Form):
    title = forms.CharField()
    image = forms.ImageField(required=False)
    description = forms.CharField(required=False)
    tags = forms.CharField(required=False)
    style = forms.CharField(initial='magazine')
    sort = forms.CharField(initial='lasttofirst')
    featured = forms.BooleanField(required=False)
    # private = forms.BooleanField(required=False)
    visibility = forms.CharField(initial='public')
