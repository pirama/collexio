import logging
from mongoengine import signals


class SubscriptionsMixin(object):
    def subscribe(self, user):
        from models import Subscriptions
        Subscriptions(
            user=user,
            collection=self
        ).save()

    def unsubscribe(self, user):
        from models import Subscriptions
        s = Subscriptions.objects.get(user=user, collection=self)
        s.delete()

    def is_subscribed(self, user):
        from models import Subscriptions
        try:
            Subscriptions.objects.get(user=user, collection=self)
            return True
        except:
            return False

    def get_subscribed_users(self):
        from models import Subscriptions
        user_list = Subscriptions.objects(collection=self).order_by("-_id")
        return user_list

    def get_subscriptions_count(self):
        from models import Subscriptions
        count = Subscriptions.objects(collection=self).count()
        return count


class SubscriptionsAccountMixin(object):
    def get_subscriptions(self):
        from collection.models import Subscriptions
        s_list = Subscriptions.objects(user=self)
        #logging.debug(s_list)

        c_list = []
        for s in s_list:
            c_list.append(s.collection)
        return c_list


class RecommendationsMixin(object):
    def recommend(self, user):
        from models import Recommendations
        Recommendations(
            user=user,
            collection=self
        ).save()

    def undorecommend(self, user):
        from models import Recommendations
        s = Recommendations.objects.get(user=user, collection=self)
        s.delete()

    def is_recommended(self, user):
        from models import Recommendations
        try:
            Recommendations.objects.get(user=user, collection=self)
            return True
        except:
            return False

    def get_recommendations_count(self):
        from models import Recommendations
        count = Recommendations.objects(collection=self).count()
        return count


class ReccommendationsAccountMixin(object):
    def get_recommended_collections(self):
        pass
