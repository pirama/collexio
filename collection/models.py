# -*- coding: utf-8 -*-
import time, logging, re, requests, json
import urlparse
from mongoengine import *
from libs.fields import ImageDict
from django.utils.timezone import now as datetime_now
from django.template.defaultfilters import slugify
from datetime import datetime
from dateutil.tz import *
from collexio.settings import *
from django.conf import settings
from bs4 import BeautifulSoup
from collaboration.mixins import CollaborationMixin
from account.models import CUser
from mixins import SubscriptionsMixin, RecommendationsMixin
from mongoengine import signals
from feed.recievers import activity_autosave_handler, activity_delete_handler, feed_privacy_update_handler
from search.recievers import post_to_es, collection_to_es, post_remove_es, collection_remove_es


class Collection(Document, CollaborationMixin, SubscriptionsMixin, RecommendationsMixin):
    title = StringField(max_length=100, required=True)
    image = ImageDict(
        thumb_tuple=(
            [1920, 0, "large"],
            [640, 860, "medium"],
            [320, 0, "small"],
            [160, 160, "thumb"]),
        required=False
    )
    description = StringField(required=False)
    created = DateTimeField()
    updated = DateTimeField()
    item_added = DateTimeField()
    slug = StringField(required=False)
    tags = ListField(required=False)
    is_featured = BooleanField(required=False)
    sort = StringField(required=True, default='lasttofirst')
    style = StringField(required=True, default='magazine')
    owner = ReferenceField(CUser, reverse_delete_rule=CASCADE)
    is_unlisted = BooleanField(required=False)
    is_private = BooleanField(required=False)

    def __unicode__(self):
        return self.title

    @property
    def get_cover(self):
        try:
            return CDN_URL+self.image['large']['filename']
        except Exception:
            return None

    @property
    def get_cover_thumb(self):
        try:
            return CDN_URL+self.image['medium']['filename']
        except Exception:
            return None

    @property
    def get_small_cover_thumb(self):
        try:
            return CDN_URL+self.image['thumb']['filename']
        except Exception:
            return None

    @property
    def get_tags(self):
        return u','.join(self.tags)

    @property
    def collexio_url(self):
        return '/c/{0}/'.format(self.slug)

    @property
    def item_count(self):
        return Item.objects(collection=self).count()

    def save(self, *args, **kwargs):
        self.updated = datetime.now(tzutc())
        if not self.slug:
            self.slug = slugify(self.title+'-'+str(int(time.time())))

        if not self.created:
            self.created = datetime.now(tzutc())

        return super(Collection, self).save(*args, **kwargs)


class Subscriptions(Document):
    user = ReferenceField(CUser, reverse_delete_rule=CASCADE)
    collection = ReferenceField(Collection, reverse_delete_rule=CASCADE)


class Recommendations(Document):
    user = ReferenceField(CUser, reverse_delete_rule=CASCADE)
    collection = ReferenceField(Collection, reverse_delete_rule=CASCADE)


class Featured(Document):
    collection = ReferenceField(Collection, reverse_delete_rule=CASCADE)
    added = DateTimeField()

    def save(self, *args, **kwargs):
        if not self.added:
            self.added = datetime.now(tzutc())

        return super(Featured, self).save(*args, **kwargs)


class Item(Document):
    title = StringField(max_length=250)
    url = URLField()
    description = StringField(required=False)
    source = StringField(required=False)
    image = ImageDict(
        thumb_tuple=(
            [960, 0, "large"],
            [640, 0, "medium"],
            [320, 200, "small"],
            [160, 160, "thumb"]),
        required=False
    )
    video = DictField(required=False)
    _card = DictField(required=False)
    added = DateTimeField()
    collection = ReferenceField(Collection, reverse_delete_rule=CASCADE)
    suggested_by = ReferenceField(CUser, required=False)
    is_accepted = BooleanField(required=False)
    added_by = ReferenceField(CUser, required=False)

    def __unicode__(self):
        return self.title

    @property
    def get_thumb(self):
        try:
            return {
                'filename': CDN_URL+self.image['thumb']['filename'],
                'w': self.image['thumb']['w'],
                'h': self.image['thumb']['h']
            }
        except Exception:
            return None

    @property
    def get_image_medium(self):
        try:
            return {
                'filename': CDN_URL+self.image['medium']['filename'],
                'w': self.image['medium']['w'],
                'h': self.image['medium']['h']
            }
        except Exception:
            return None

    @property
    def get_image_original(self):
        try:
            return {
                'filename': CDN_URL+self.image['original']['filename'],
                'w': self.image['original']['w'],
                'h': self.image['original']['h']
            }
        except Exception:
            return None

    @property
    def card(self):
        if self._card:
            return self._card

        if self.url:
            for p in embedly_providers:
                if p in self.url:
                    card = {}
                    card['type'] = "rich"
                    card['html'] = '<a href="'+self.url+'" class="embedly-url hidden">'+self.url+'</a>'
                    return card

            for k, v in dyn_gen.items():
                if re.match(v, self.url):
                    klass = globals()[k]
                    c = klass(self.url).get_gen_card()
                    return c

        else:
            return None

    @property
    def has_card(self):
        if self.get_image_medium or self.card:
            return True
        else:
            return False


    @property
    def collexio_url(self):
        return collection.url+'#'+self.id

    @property
    def user(self):
        if self.added_by:
            return self.added_by
        else:
            return self.collection.owner

    def save(self, *args, **kwargs):

        if not self.added:
            self.added = datetime.now(tzutc())

        self.collection.item_added = self.added
        self.collection.save()

        return super(Item, self).save(*args, **kwargs)


def get_url_data(url):
    for k, v in special_cards.items():
        if re.match(v, url):
            klass = globals()[k]
            return klass(url).get_item_data()

    for p in oembed_providers:
        if p in url:
            return OEmbedItem(url).get_item_data()

    for p in embedly_providers:
        if p in url:
            return EmbedlyItem(url).get_item_data()

    return ItemData(url).get_item_data()


special_cards = {
    'VineVideo': '\S*vine.co/v',
    'TwitterTweet': '\S*twitter\.com/\w+/status/\d+'
}

dyn_gen = {
    'VineVideo': '\S*vine.co/v',
    'YoutubeVideo': '\S*youtube.com/watch',
    'VimeoVideo': '\S*vimeo.com/\d+',
}
oembed_providers = ['flickr.com', 'youtube.com', 'vimeo.com', 'twitter.com']
embedly_providers = ['amazon.com']


class ItemData(object):
    def __init__(self, url):
        self.url = url

    def get_item_data(self):
        self.soup = self.get_soup()
        self.item_data = {}
        self.item_data['url'] = self.url
        self.item_data['card'] = self.get_card_data()
        self.item_data['preview'] = self.get_preview()
        self.item_data.update(self.get_item_details())
        self.item_data.pop('og_meta', None)
        self.item_data.pop('video', None)
        self.item_data.pop('page_meta', None)
        return self.item_data

    def get_soup(self):
        try:
            headers = {
                "User-Agent": "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"
            }
            response = requests.get(self.url, headers=headers, allow_redirects=True)
            html = response.content
            return BeautifulSoup(html, "html5lib")
        except Exception:
            return None

    def get_card_data(self):
        return {}

    def get_preview(self):
        return self.get_card_data()

    def get_gen_card(self):
        return None

    def get_item_details(self):
        data = {}
        parsed = urlparse.urlparse(self.url)

        try:
            data['page_meta'] = self.page_meta()
        except Exception:
            return data

        try:
            data['og_meta'] = self.og_meta()

            #image
            if 'image' in data['og_meta']:
                data['image'] = data['og_meta']['image']

            if 'image:url' in data['og_meta']:
                data['image'] = data['og_meta']['image:url']
        except Exception:
            pass

        data['source'] = parsed.netloc

        if 'title' in data['og_meta']:
            data['title'] = data['og_meta']['title']
        elif 'title' in data['page_meta']:
            data['title'] = data['page_meta']['title']

        if 'description' in data['og_meta']:
            data['description'] = data['og_meta']['description']
        elif 'description' in data['page_meta']:
            data['description'] = data['page_meta']['description']

        return data

    def page_meta(self):
        meta = {}
        description_tag = self.soup.find("meta", {"name": "description"})

        if description_tag:
            meta['description'] = description_tag['content']

        if self.soup.title:
            meta['title'] = self.soup.title.string

        return meta

    def og_meta(self):
        meta = {}
        soup_meta = self.soup.find_all("meta", property=re.compile("og:"))
        for og_meta in soup_meta:
            meta[og_meta['property'].replace('og:', '')] = og_meta['content']

        return meta


class OEmbedItem(ItemData):
    def get_card_data(self):
        link = self.soup.find(type="application/json+oembed")
        if link:
            response = requests.get(link['href'])
            oEmbed_data = json.loads(response.content)
        else:
            oEmbed_data = {}

        return oEmbed_data


class EmbedlyItem(ItemData):
    def get_card_data(self):
        return {}

    def get_preview(self):
        card = {}
        card['type'] = "embedly"
        card['html'] = '<a href="'+self.url+'" class="embedly-url hidden">'+self.url+'</a>'
        return card

    """
    def get_card_data(self):
        oembed_url = "http://api.embed.ly/1/oembed?key=1457678e02504979b6c7eb1170968ddd&format=json&url="+self.url
        response = requests.get(oembed_url)
        return response.content
    """


class VineVideo(ItemData):
    def get_card_data(self):
        url = str(self.url)
        url = url.replace('http://', '//')
        url = url.replace('https://', '//')
        url = url+'/embed/simple'
        embed_url = url
        html = '<iframe class="vine-embed" width="640" height="640" src="'+embed_url+'" frameborder="0" allowfullscreen"></iframe>'
        card = {}
        card['type'] = "video"
        card['html'] = html
        return card

    def get_gen_card(self):
        return get_card_data()


class YoutubeVideo(OEmbedItem):
    def get_gen_card(self):
        res = urlparse.urlparse(self.url)
        q_dict = urlparse.parse_qs(res.query)
        embed_url = '//youtube.com/embed/'+q_dict['v'][0]
        html = '<iframe class="youtube-embed" width="640" height="640" src="'+embed_url+'" frameborder="0" allowfullscreen"></iframe>'
        card = {}
        card['type'] = "video"
        card['html'] = html
        return card


class VimeoVideo(OEmbedItem):
    def get_gen_card(self):
        url = str(self.url)
        url = url.replace('http://', '//')
        url = url.replace('https://', '//')
        url = url.replace('vimeo.com', 'player.vimeo.com/video')
        embed_url = url
        html = '<iframe class="vimeo-embed" width="100%" height="100%" src="'+embed_url+'" frameborder="0" allowfullscreen"></iframe>'
        card = {}
        card['type'] = "video"
        card['html'] = html
        return card


class TwitterTweet(OEmbedItem):
    def get_item_details(self):
        data = {}
        parsed = urlparse.urlparse(self.url)
        data['source'] = parsed.netloc
        tweet_soup = BeautifulSoup(self.item_data['card']['html'])
        data['as_text'] = tweet_soup.get_text(strip=True)
        return data

# Signals for feed

signals.post_save.connect(activity_autosave_handler, sender=Subscriptions)
signals.post_save.connect(activity_autosave_handler, sender=Recommendations)
signals.post_save.connect(activity_autosave_handler, sender=Item)

signals.post_delete.connect(activity_delete_handler, sender=Item)
signals.post_delete.connect(activity_delete_handler, sender=Subscriptions)
signals.post_delete.connect(activity_delete_handler, sender=Recommendations)

signals.post_save.connect(feed_privacy_update_handler, sender=Collection)

# Signals for Elasticsearch update

signals.post_save.connect(post_to_es, sender=Item)
signals.post_save.connect(collection_to_es, sender=Collection)

signals.post_delete.connect(post_remove_es, sender=Item)
signals.post_delete.connect(collection_remove_es, sender=Collection)



