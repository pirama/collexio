# -*- coding: utf-8 -*-
import logging
import json, traceback
import uuid, imghdr, re, requests, urlparse, cStringIO, logging, html5lib
from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.template import RequestContext
from django.utils.html import escape
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.clickjacking import xframe_options_exempt
from django.core.paginator import Paginator
from datetime import datetime
from models import *
from views import add_edit_item, accept_item, decline_item
from forms import *
from notifications.models import *
from collection.serialize import *
from collexio.utils.rest import json_request

@csrf_exempt
@xframe_options_exempt
def item(request, collection_id=None, item_id=None):
    collection = None
    data_dict = {}
    data_dict['item_id'] = item_id
    data_dict['messages'] = {}
    data_dict['messages']['debug'] = []

    if request.method == 'POST':
        try:
            form = ItemForm(request.POST)
            if form.is_valid():

                data_dict['messages']['debug'].append('form valid')

                if collection_id:
                    collection = Collection.objects.get(pk=collection_id)

                    if not collection.check_permission(request.user, "canadd"):
                        data_dict['messages']['error'] = "You don't have permission for this."

                        return HttpResponse(
                            json.dumps(data_dict),
                            content_type='application/javascript; charset=utf8'
                        )

                item_data = form.cleaned_data
                #data_dict['messages']['debug'].append(item_data)

                if 'media_removed' in request.POST:
                    item_data['media_removed'] = request.POST['media_removed']

                if item_id:
                    item_data['id'] = item_id
                    item = Item.objects.get(pk=item_id)
                    collection = item.collection
                    data_dict['action'] = "edit"
                else:
                    item_data['added_by'] = request.user

                if request.user.is_authenticated():
                    if item_data['suggested'] == "True":
                        item_data['suggested_by'] = request.user
                        item = add_edit_item(item_data, collection)
                        data_dict['messages']['success'] = 'Your suggestion has beed sent.'
                        data_dict['item'] = {
                            'id': str(item.pk),
                            'title': item.title,
                            'url': item.url,
                            'description': item.description
                        }

                        SuggestionNotification(
                            to=collection.owner,
                            nt_from=request.user,
                            item=item
                        ).send_nt()
                    else:
                        try:
                            item = add_edit_item(item_data, collection)
                            data_dict['messages']['success'] = 'Your item has beed saved.'
                            data_dict['item'] = {
                                'id': str(item.pk),
                                'title': item.title,
                                'url': item.url,
                                'description': item.description
                            }
                        except Exception, e:
                            data_dict['messages']['error'] = 'Something went wrong when adding item. -Data Error.'
                            data_dict['messages']['error_text'] = str(e)
                else:
                    data_dict['messages']['error'] = 'Something went wrong when adding item. -Auth Error.'
            else:
                data_dict['messages']['error'] = 'Something went wrong when adding item. - Invalid Data.'
                data_dict['messages']['errors'] = form.errors

        except Exception, e:
            data_dict['messages']['error2'] = 'Something went wrong when adding item. Unexpected Error.'
            data_dict['messages']['error_text'] = str(e)
    else:
        item = Item.objects.get(pk=item_id)
        data_dict['item'] = {
            'id': str(item.pk),
            'title': item.title,
            'url': item.url,
            'description': item.description,
            'preview': item.card,
            'source': item.source,
            'card': item._card,
        }
        if item.get_image_original:
            data_dict['item']['image'] = item.get_image_original['filename']

    return HttpResponse(
        json.dumps(data_dict),
        content_type='application/javascript; charset=utf8'
    )


def item_image_auto_upload(request):
    data_dict = {}
    try:
        file_object = request.FILES['image']

        """ Upload image and return the path"""

        ext = os.path.splitext(file_object.name)[1]
        file_name = uuid.uuid4().hex
        base = MEDIA_ROOT
        part = 'uploads/images/{}{}'.format(file_name, ext)
        file_name = file_name+ext
        file_path = base+part
        file_url = MEDIA_URL+part
        with open(file_path, 'wb+') as destination:
            for chunk in file_object.chunks():
                destination.write(chunk)

        data_dict['file_path'] = part
        data_dict['file_url'] = file_url

    except Exception, e:
        data_dict['message'] = str(e)

    return HttpResponse(
        json.dumps(data_dict),
        content_type='application/javascript; charset=utf8'
    )


def get_site_content(request):
    url = request.GET.get('url', None)
    item_data = get_url_data(url)
    if url:
        return HttpResponse(
            json.dumps(item_data),
            content_type='application/javascript; charset=utf8'
        )

    else:
        return None


def get_collections(request):
    data = {}
    data['messages'] = {}
    if not request.user.is_authenticated():
        data['messages']['error'] = "Not Authenticated"
    else:
        c_list = []
        collections = Collection.objects(
            Q(collaborators__user=request.user) |
            Q(owner=request.user)
        ).filter(
            Q(owner=request.user) | (Q(collaborators__permissions="admin") | Q(collaborators__permissions="canadd"))
        ).order_by('-item_added')

        for collection in collections:
            if collection.owner != request.user:
                title = collection.title+" *"
            else:
                title = collection.title

            c_list.append({
                'id': str(collection.pk),
                'title': title,
                'slug': collection.slug,
                'owner': collection.owner.username
            })
            data['c_list'] = c_list

    return HttpResponse(
        json.dumps(data),
        content_type='application/javascript; charset=utf8'
    )


def item_list_ajax(request, collection_id):
    from django.template.loader import get_template
    from django.template import Context

    collection = Collection.objects.get(pk=collection_id)
    if collection.is_private and collection.owner != request.user:
        raise Http404

    if collection.sort == 'firsttolast':
        order = 'added'
    else:
        order = '-added'

    i_list = Item.objects.filter((Q(suggested_by__exists=False) | Q(is_accepted=True)), collection=collection).order_by(order)
    p = Paginator(i_list, 10)
    r_page = request.GET.get('p', 1)
    item_list = p.page(r_page)

    html_content = ""
    for i in item_list:
        if collection.style == "list":
            htmly = get_template("list_item_layout.html")
        elif collection.style == "pinboard":
            htmly = get_template("pinboard_item_layout.html")
        else:
            htmly = get_template("magazine_item_layout.html")
        ctx = {
            'item': i,
            'collection': collection,
            'user': request.user
        }
        d = Context(ctx)
        html_content += htmly.render(d)

    data = {}
    data['html'] = json.dumps(html_content)
    data['p'] = {}
    data['p']['count'] = p.count
    data['p']['range'] = p.page_range
    data['p']['page'] = r_page
    data['p']['has_next'] = item_list.has_next()

    if item_list.has_next():
        data['p']['next_page_number'] = item_list.next_page_number()

    return HttpResponse(
        json.dumps(data),
        content_type='application/javascript; charset=utf8'
    )


def item_html(request, item_id):
    item = Item.objects.get(pk=item_id)

    ctx = {'item': item, 'collection': item.collection}
    return render_to_response('magazine_item_layout.html', ctx, context_instance=RequestContext(request))


def item_accepted(request, item_id=None):
    data = {}
    data['messages'] = {}

    try:
        accept_item(request, item_id)
        data['messages']['success'] = "Accepted"
    except Exception:
        data['messages']['error'] = "failed"

    return HttpResponse(
        json.dumps(data),
        content_type='application/javascript; charset=utf8'
    )


def item_nevermind(request, item_id=None):
    data = {}
    data['messages'] = {}

    try:
        decline_item(request, item_id)
        data['messages']['success'] = "Accepted"
    except Exception:
        data['messages']['error'] = "failed"

    return HttpResponse(
        json.dumps(data),
        content_type='application/javascript; charset=utf8'
    )


def subscribe(request, collection_id):
    data = {}
    data['messages'] = {}
    c = Collection.objects.get(pk=collection_id)
    c.subscribe(request.user)
    """
    try:
        c.subscribe(request.user)
        data['status'] = "success"
    except:
        data['status'] = "error"
        data['messages']['error'] = 'Something went wrong when subscribing this collection'
    """
    return HttpResponse(
        json.dumps(data),
        content_type='application/javascript; charset=utf8'
    )


def unsubscribe(request, collection_id):
    data = {}
    data['messages'] = {}
    c = Collection.objects.get(pk=collection_id)
    try:
        c.unsubscribe(request.user)
        data['status'] = "success"
    except:
        data['messages']['error'] = 'Something went wrong when unsubscribing this collection'

    return HttpResponse(
        json.dumps(data),
        content_type='application/javascript; charset=utf8'
    )


def recommend(request, collection_id):
    data={}
    data['messages'] = {}
    c = Collection.objects.get(pk=collection_id)
    c.recommend(request.user)
    """
    try:
        c.recommend(request.user)
        data['status'] = "success"
    except:
        data['messages']['error'] = 'Something went wrong when recommending this collection'
    """
    return HttpResponse(
        json.dumps(data),
        content_type='application/javascript; charset=utf8'
    )


def undorecommend(request, collection_id):
    data={}
    data['messages'] = {}
    c = Collection.objects.get(pk=collection_id)
    c.undorecommend(request.user)

    """    
    try:
        
    except:
        data['messages']['error'] = 'Something went wrong when undo recommending this collection'
    """
    return HttpResponse(
        json.dumps(data),
        content_type='application/javascript; charset=utf8'
    )

