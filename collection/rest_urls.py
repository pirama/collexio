from django.conf.urls import patterns, include, url

# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^(?P<collection_id>\w+)/add/$', 'collection.rest.item'),
    url(r'^(?P<collection_id>\w+)/items/$', 'collection.rest.item_list_ajax'),
    url(r'^(?P<collection_id>\w+)/subscribe/$', 'collection.rest.subscribe'),
    url(r'^(?P<collection_id>\w+)/unsubscribe/$', 'collection.rest.unsubscribe'),
    url(r'^(?P<collection_id>\w+)/recommend/$', 'collection.rest.recommend'),
    url(r'^(?P<collection_id>\w+)/undorecommend/$', 'collection.rest.undorecommend'),
    url(r'^', include('collaboration.rest_urls')),
)
