def serialize_user(profile):
    return {
        'date_joined': str(profile.date_joined),
        'description': profile.description,
        'id': str(profile.id),
        'image': profile.image,
        'name': profile.name,
        'username': profile.username
    }


def serialize_collection(collection):
    return {
        'id': str(collection.pk),
        'title': collection.title,
        'slug': collection.slug
    }


def serialize_item(item):
    return {
        'id': str(item.id),
        'added': str(item.added),
        'title': item.title,
        'image': item.image,
        'video': item.video,
        'url': item.url,
        'description': item.description,
        'collection': serialize_collection(item.collection),
        'suggested_by': serialize_user(item.suggested_by)
    }


def serialize_notification(nt):
    s = {
        'id': str(nt.id),
        'to': serialize_user(nt.to),
        'from': serialize_user(nt.nt_from),
        'type': nt._cls,
        'message': nt.message,
        'is_seen': nt.is_seen,
        'created': nt.created.isoformat()+"Z"
    }

    if 'item' in nt:
        s['item'] = serialize_item(nt.item)
        s['is_accepted'] = nt.item.is_accepted
        s['collection'] = serialize_collection(nt.item.collection)

    return s
