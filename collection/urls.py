from django.conf.urls import patterns, url, include

urlpatterns = patterns(
    '',
    url(r'^create/$', 'collection.views.create', name='create-collection'),
    url(r'^ext/$', 'collection.views.ext', name='extension'),
    url(r'^full/$', 'collection.views.full_list', name='full-list'),
    url(r'^add_item_ext/$', 'collection.views.add_item_ext', name='add_item_ext'),
    url(r'^get_site_content/$', 'collection.rest.get_site_content', name='get_site_content'),
    url(r'^featured/$', 'collection.views.featured', name='featured'),
    url(r'^list/(?P<list_type>[-\w]+)/$', 'collection.views.collection_list_ajax', name='collection_list'),
    url(r'^list/(?P<list_type>[-\w]+)/(?P<profile_id>[-\w]+)/$', 'collection.views.collection_list_ajax', name='collection_list'),

    url(r'^(?P<slug>[-\w]+)/$', 'collection.views.detail', name='collection'),
    url(r'^(?P<collection_id>[-\w]+)/edit/$', 'collection.views.edit', name='collection-edit'),
    url(r'^(?P<collection_id>[-\w]+)/delete/$', 'collection.views.delete_collection', name='collection-delete'),

    url(r'^', include('collaboration.urls')),

    url(r'^i/(?P<item_id>[-\w]+)/delete/$', 'collection.views.delete_item', name='item-delete'),
    url(r'^i/(?P<item_id>[-\w]+)/accept/$', 'collection.views.accept_item_view', name='item-accept'),
    url(r'^i/(?P<item_id>[-\w]+)/decline/$', 'collection.views.decline_item_view', name='item-decline'),
)


"""
    url(r'^(?P<collection_id>[-\w]+)/add_item/$', 'collection.views.add_item_web', name='add-item'),
    url(r'^(?P<item_id>[-\w]+)/edit_item/$', 'collection.views.edit_item', name='item-edit'),
"""
