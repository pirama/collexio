# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, Http404
from django.template import RequestContext
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.views.decorators.clickjacking import xframe_options_exempt
from django.core.paginator import Paginator

from account.models import Following
from notifications.models import *
from forms import *
from models import *
from feed import utils as feed_utils

import requests
from elasticsearch import Elasticsearch
from search.decorators import check_content_type
from bson.objectid import ObjectId



def collection_list(request, list_type, profile=None, pagination=True):
    if list_type == "feed":
        followings = Following.objects.filter(user=request.user)
        f_list = []
        for rec in followings:
            f_list.append(rec.following)

        collections = Collection.objects(owner__in=f_list, is_private__ne=True, is_unlisted__ne=True).order_by('-created')

    elif list_type == "featured":
        collections = []
        f_list = Featured.objects().order_by('-added')
        for f in f_list:
            collections.append(f.collection)

    # collections = Collection.objects(is_featured=True).order_by('-created')

    elif list_type == "public":
        if not profile:
            profile = CUser.objects.get(pk=request.GET['a'])

        collections = Collection.objects(owner=profile, is_private__ne=True, is_unlisted__ne=True).order_by('-item_added')

    elif list_type == "private":
        if not profile:
            profile = CUser.objects.get(pk=request.GET['a'])

        collections = Collection.objects(owner=profile, is_private=True).order_by('-item_added')

    elif list_type == "unlisted":
        if not profile:
            profile = CUser.objects.get(pk=request.GET['a'])

        collections = Collection.objects(owner=profile, is_unlisted=True).order_by('-item_added')
    print 'list_type', list_type
    p = Paginator(collections, 18)
    r_page = request.GET.get('p', 1)
    c_list = p.page(r_page)

    return c_list


def collection_list_ajax(request, list_type):
    c_list = collection_list(request, list_type)
    ctx = {'collections': c_list, 'tab': list_type}
    return render_to_response('collection_list.html', ctx, context_instance=RequestContext(request))


def main(request):
    ctx = {}
    if request.user.is_authenticated():
        activities = feed_utils.get_feed(user=request.user, count=20)

        collections = Collection.objects(is_featured=True, is_private__ne=True).order_by('-created')
        p_collections = Paginator(collections, 3)
        ctx = {
            'activity_list': activities,
            'featured_collections': p_collections.page(1),
            'featured_collections2': p_collections.page(2),
            'tab': 'feed'
        }
        return render_to_response('feed.html', ctx, context_instance=RequestContext(request))

        """
        if request.user.following_count >= 1:
            c_list = collection_list(request, "feed")
            ctx = {'collections': c_list, 'tab': 'feed'}
        else:
            c_list = collection_list(request, "featured")
            ctx = {'collections': c_list, 'tab': 'featured'}
        """
    else:
        return render_to_response('index.html', ctx, context_instance=RequestContext(request))


def featured(request):
    c_list = collection_list(request, "featured")
    ctx = {'collections': c_list, 'tab': 'featured'}
    return render_to_response('index.html', ctx, context_instance=RequestContext(request))


@login_required(login_url=settings.LOGIN_URL)
def create(request):

    if request.method == 'POST':
        form = CollectionForm(request.POST, request.FILES)
        if form.is_valid():
            tag_list = form.cleaned_data['tags'].split(',')

            collection = Collection(
                title=form.cleaned_data['title'],
                description=form.cleaned_data['description'],
                tags=tag_list,
                owner=request.user,
                sort=form.cleaned_data['sort'],
                is_featured=form.cleaned_data['featured'],
                # is_private=form.cleaned_data['private']
            )

            collection.save()

            visibility = form.cleaned_data['visibility']
            if visibility == 'private':
                collection.is_private = True
            elif visibility == 'unlisted':
                collection.is_unlisted = True

            collection.save()
            collection.image = form.cleaned_data['image']
            collection.save()

            NewCollectionNotification(
                nt_from=request.user,
                collection=collection
            ).send_nt()

            return HttpResponseRedirect(reverse('collection', args=(collection.slug,)))
        else:
            messages.add_message(request, messages.ERROR, 'Something went wrong when creating your new collection .')
    else:
        form = CollectionForm()

    context_dict = {'form': form}

    return render_to_response('create.html', context_dict, context_instance=RequestContext(request))


@login_required(login_url=settings.LOGIN_URL)
def edit(request, collection_id):
    collection = Collection.objects.get(pk=collection_id)

    if not collection.check_permission(request.user, "admin"):
        messages.add_message(request, messages.ERROR, "You don't have permission for this.")
        return HttpResponseRedirect(reverse('collection', args=(collection.slug,)))


    if request.method == 'POST':
        form = CollectionForm(request.POST, request.FILES)
        if form.is_valid():
            tag_list = form.cleaned_data['tags'].split(',')

            collection.title = form.cleaned_data['title']
            collection.description = form.cleaned_data['description']
            collection.tags = tag_list
            collection.is_featured = form.cleaned_data['featured']
            collection.style = form.cleaned_data['style']
            collection.sort = form.cleaned_data['sort']
            # collection.is_private = form.cleaned_data['private']

            visibility = form.cleaned_data['visibility']
            collection.is_private = False
            collection.is_unlisted = False
            if visibility == 'private':
                collection.is_private = True
            elif visibility == 'unlisted':
                collection.is_unlisted = True

            collection.save()

            if 'image' in request.FILES:
                if request.FILES['image']:
                    collection.image = form.cleaned_data['image']
                    collection.save()

            """
            Add item to featured
            """

            if collection.is_featured:
                try:
                    featured = Featured.objects.get(collection=collection)
                except:
                    featured = Featured(collection=collection)
                    featured.save()
            else:
                try:
                    featured = Featured.objects.get(collection=collection)
                    featured.delete()
                except:
                    pass

            return HttpResponseRedirect(reverse('collection', args=(collection.slug,)))
        else:
            logging.debug(form.errors)
            messages.add_message(request, messages.ERROR, 'Something went wrong when editing your collection .')
    else:
        visibility = 'public'
        if collection.is_private:
            visibility = 'private'
        elif collection.is_unlisted:
            visibility = 'unlisted'
        form = CollectionForm({
            'title': collection.title,
            'description': collection.description,
            'tags': collection.get_tags,
            'sort': collection.sort,
            'style': collection.style,
            'visibility': visibility
        })

    ctx = {'form': form, 'collection': collection, 'collection_edit': True}

    return render_to_response('create.html', ctx, context_instance=RequestContext(request))


def get_embed_url(item_url):
    res = urlparse.urlparse(item_url)
    q_dict = urlparse.parse_qs(res.query)
    return q_dict['v']


def add_edit_item(item_data, collection=None):
    if 'id' in item_data:
        item = Item.objects.get(pk=item_data['id'])
    else:
        item = Item(collection=collection)

    if 'url' in item_data:
        if item_data['url'] != '':
            item.url = item_data['url']

    if 'title' in item_data:
        item.title = item_data['title']

    if 'description' in item_data:
        item.description = item_data['description']

    if 'source' in item_data:
        item.source = item_data['source']

    if 'card_data' in item_data:
        if item_data['card_data'] != "":
            item._card = json.loads(item_data['card_data'])

    if 'preview' in item_data:
        item._card = item_data['preview']

    if 'added_by' in item_data:
        item.added_by = item_data['added_by']

    if 'image_url' in item_data:
        if item_data['image_url']:
            item.image = item_data['image_url']

    if 'image' in item_data:
        item.image = item_data['image']

    if 'media_removed' in item_data:
        if item_data['media_removed'] == "True":
            if 'image' in item:
                item.image = None

    if 'suggested_by' in item_data:
        item.suggested_by = item_data['suggested_by']

    item.save()

    return item


@login_required(login_url=settings.LOGIN_URL)
def delete_collection(request, collection_id):
    collection = Collection.objects.get(id=collection_id)
    if not collection.check_permission(request.user, "admin"):
        messages.add_message(request, messages.ERROR, "You don't have permission for this.")
        return HttpResponseRedirect(reverse('collection', args=(collection.slug,)))

    collection.delete()
    return HttpResponseRedirect(reverse('profile', args=(request.user.username,)))


@login_required(login_url=settings.LOGIN_URL)
def delete_item(request, item_id):
    item = Item.objects.get(pk=item_id)
    collection = item.collection

    if not collection.check_permission(request.user, "admin"):
        messages.add_message(request, messages.ERROR, "You don't have permission for this.")
        return HttpResponseRedirect(reverse('collection', args=(collection.slug,)))
    else:
        item.delete()

    return HttpResponseRedirect(reverse('collection', args=(item.collection.slug,)))


def accept_item(request, item_id=None):
    item = Item.objects.get(pk=item_id)

    if request.user == item.collection.owner:
        try:
            nt = Notifications.objects.get(item=item)
            nt.is_accepted = True
            nt._is_seen = True
            item.is_accepted = True
            item.save()
            nt.save()
            return item
        except Exception, e:
            raise e

    else:
        raise ValueError("failed")


def decline_item(request, item_id=None):
    item = Item.objects.get(pk=item_id)

    if request.user == item.collection.owner:
        try:
            nt = Notifications.objects.get(item=item)
            nt.is_accepted = False
            nt._is_seen = True
            item.is_accepted = False
            item.save()
            nt.save()
            return item
        except Exception, e:
            raise e
    else:
        raise ValueError('failed')


@login_required(login_url=settings.LOGIN_URL)
def accept_item_view(request, item_id=None):
    try:
        item = accept_item(request, item_id)
        messages.add_message(request, messages.SUCCESS, 'Suggested item added to your collection.')
    except Exception:
        messages.add_message(request, messages.ERROR, 'Something went wrong when adding suggested item to your collection.')

    return HttpResponseRedirect(reverse('collection', args=(item.collection.slug,)))


@login_required(login_url=settings.LOGIN_URL)
def decline_item_view(request, item_id=None):
    try:
        item = decline_item(request, item_id)
        messages.add_message(request, messages.SUCCESS, 'You declined the suggested item.')
    except Exception:
        messages.add_message(request, messages.ERROR, 'Something went wrong when declining suggested item.')

    return HttpResponseRedirect(reverse('collection', args=(item.collection.slug,)))


def help(request):
    return render_to_response('help.html', context_instance=RequestContext(request))


def detail(request, slug):
    form = ItemForm()

    if slug is not None:
        try:
            collection = Collection.objects.get(slug=slug)
        except Exception:
            raise Http404

        if not collection.check_permission(request.user, "canview"):
            #messages.add_message(request, messages.ERROR, "Permission Error")
            raise Http404

        is_following = None
        if request.user.is_authenticated():
            is_following = str(request.user.is_following(collection.owner))

        if collection.sort == 'firsttolast':
            order = 'added'
        else:
            order = '-added'

        i_list = Item.objects.filter((Q(suggested_by__exists=False) | Q(is_accepted=True)), collection=collection).order_by(order)
        p = Paginator(i_list, 10)
        r_page = request.GET.get('p', 1)
        item_list = p.page(r_page)
        logging.debug(p.count)

        """
        recommendation_count = Item.objects.filter(collection=collection, suggested_by__exists=True).order_by(order).count()
        recommended_items = Item.objects.filter(collection=collection, suggested_by__exists=True).order_by(order)
        """

    style = request.GET.get('style', None)

    if style == 'gallery':
        collection.style = "gallery"

    collections = Collection.objects(is_featured=True).order_by('-created')[:6]

    context_dict = {
        'collection': collection,
        'item_list': item_list,
        'form': form,
        'collection_page': True,
        'collections': collections,
        'is_following': is_following,
        'is_subscribed': collection.is_subscribed(request.user),
        'is_recommended': collection.is_recommended(request.user),
        'can_add_item': collection.check_permission(request.user, "canadd"),
        'can_edit': collection.check_permission(request.user, "admin"),

    }

    return render_to_response('collection.html', context_dict, context_instance=RequestContext(request))


@xframe_options_exempt
def ext(request):
    url = request.GET.get('url', None)
    collection_list = Collection.objects(owner=request.user)
    site_content = get_url_data(url)
    context_dict = {
        'site_content': json.dumps(site_content),
        'url': url,
        'collection_list': collection_list
    }
    return render_to_response('extension.html', context_dict, context_instance=RequestContext(request))


@xframe_options_exempt
def add_item_ext(request):
    url = request.GET.get('url', None)
    collection_id = request.GET.get('collection_id', None)
    collection = Collection.objects.get(pk=collection_id)
    item = Item(
        url=url,
        collection=collection
    )
    item.save()
    context_dict = {'collection': collection}
    return render_to_response('ext_done.html', context_dict, context_instance=RequestContext(request))


def full_list(request):
    if not request.user.is_superuser:
        raise Http404

    collections = Collection.objects.all().order_by('-created')
    ctx = {'collections': collections}
    return render_to_response('full_list.html', ctx, context_instance=RequestContext(request))


def fetch_og_data(url):
    import opengraph  # https://pypi.python.org/pypi/opengraph
    og_data = opengraph.OpenGraph(url=url)
    data = {}
    if og_data:

        #general
        if 'site_name' in og_data:
            data['source'] = og_data['site_name']

        if 'description' in og_data:
            data['description'] = og_data['description']

        if 'title' in og_data:
            data['title'] = og_data['title']

        #image
        if 'image' in og_data:
            data['image'] = og_data['image']

        if 'image:url' in og_data:
            data['image'] = og_data['image:url']

        #video
        video = dict()
        if 'video' in og_data:
            video['video_url'] = og_data['video']

        if 'video:url' in og_data:
            video['video_url'] = og_data['video:url']

        if 'video:width' in og_data:
            video['video_width'] = og_data['video:width']

        if 'video:height' in og_data:
            video['video_height'] = og_data['video:height']

        if 'video:type' in og_data:
            video['video_type'] = og_data['video:type']

        data['video'] = video

        return data


def email_test(request, template):
    item = Item.objects.get(id="53b2a7f7067a771fe27ad304")
    follower = CUser.objects.get(id=ObjectId("53ec8bdd96540a3411ffa39e"))
    user = CUser.objects.get(id=ObjectId("53b1112ae7798909989dce60"))
    ctx = {'item': item, 'collection': item.collection, 'user':user, 'follower':follower}
    return render_to_response('emails/'+template+'.html', ctx, context_instance=RequestContext(request))


def generate_search_data(search_query, content_type=False, only_my=False, username=False):
    # query structure for ElasticSearch
    filtered = {}
    filtered['query'] = {}
    filtered['query']['query_string'] = {}
    filtered['query']['query_string']['query'] = search_query
    filtered['filter'] = {}
    filtered['filter']['bool'] = {}
    filtered['filter']['bool']['must_not'] = [{"term": {"is_private": True}}, {"term": {"is_unlisted": True}}]
    filtered['filter']['bool']['must'] = []

    search_data = {}

    if search_query:
        if not (only_my or content_type):
            search_data = {
                "query": {"filtered": filtered}
            }
        elif only_my:
            profile = CUser.objects.get(username=username)
            filtered['filter']['bool']['must'] = [{"term": {"owner": str(profile.pk)}}, {"term": {"_type": content_type}}]
            search_data = {
                "query": {"filtered": filtered}
            }
        else:
            filtered['filter']['bool']['must'] = [{"term": {"_type": content_type}}]
            search_data = {
                "query": {"filtered": filtered}
            }
    return search_data


def test_search(request, content_type=False):
    search_query = request.GET.get('q', False)  # search query
    only_my = request.GET.get('only_my', False)  # search on your collections
    username = False

    if request.user.is_authenticated():
        username = request.user.username

    if search_query:
        search_data = generate_search_data(search_query, content_type, only_my, username)
        result = requests.post(settings.ES_URL, data=json.dumps(search_data)).json()
        if 'status' in result:
            return HttpResponse(json.dumps({'error': result['error']}))
        elif 'hits' in result:
            if 'total' in result['hits']:
                if result['hits'].get('total') > 0:
                    return HttpResponse(json.dumps(result))
        return HttpResponse(json.dumps({'error': 'Empty result'}))
    else:
        return HttpResponse('Search query is empty')


def autocomplete_serializer(raw_data=False):
    data = []
    if raw_data:
        for item in raw_data:
            data.append({
                'title': item.title,
                'author': item.owner.username,
                'url':  item.collexio_url,
                'thumb': item.get_cover_thumb,
                'created': (item.created).strftime("%b %d")
            })
    return data


@check_content_type(allowed_types=['collection'])
def search_autocomplete(request, content_type='collection'):
    search_query = request.GET.get('q', False)  # search query
    result = {}

    if search_query:
        search_query = [search_query, "{0}*".format(search_query), "*{0}*".format(search_query)]
        for q in search_query:
            search_data = generate_search_data(q, content_type, only_my=False, username=False)
            es = Elasticsearch([settings.ES_URL])
            raw_result = es.search(
                index=['collexio'],
                fields=['_id'],
                doc_type=[content_type],
                body=json.dumps(search_data)
            )

            if 'hits' in raw_result:
                if len(raw_result['hits']['hits']) > 0:
                    pks = []
                    for item in raw_result['hits']['hits']:
                        pks.append(item['_id'])

                    result['data'] = Collection.objects.filter(pk__in=pks).order_by('-added')
                    break

        if 'data' not in result:
            return HttpResponse('Wrong result')  # TODO: Create template

    return HttpResponse(json.dumps(autocomplete_serializer(result['data'])))


@check_content_type()
def search(request, content_type=False):

    search_query = request.GET.get('q', False)  # search query
    only_my = request.GET.get('only_my', False)  # search on your collections
    username = False
    result = {}

    if request.user.is_authenticated():
        username = request.user.username

    if search_query:
        # Do many searches until success. Strict search first.
        search_queries = [search_query, "*{0}*".format(search_query)]
        for q in search_queries:

            search_data = generate_search_data(q, content_type, only_my, username)
            es = Elasticsearch([settings.ES_URL])
            raw_result = es.search(
                index=['collexio'],
                fields=['_id'],
                doc_type=[content_type],
                body=json.dumps(search_data)
            )

            if 'hits' in raw_result:
                if len(raw_result['hits']['hits']) > 0:
                    pks = []
                    for item in raw_result['hits']['hits']:
                        pks.append(item['_id'])

                    if content_type == 'item':
                        result['data'] = Item.objects.filter(pk__in=pks).order_by('-added')
                    elif content_type == 'collection':
                        result['data'] = Collection.objects.filter(pk__in=pks).order_by('-added')
                    else:
                        result['data'] = CUser.objects.filter(pk__in=pks)
                    break

        if 'data' not in result:
            return HttpResponse('Wrong result')  # TODO: Create template
    else:
        return HttpResponse('Search query is empty')  # TODO: Create template

    context_dict = {
        "result": result,
        "type": content_type,
        "q": search_query,
        "only_my": only_my
    }

    return render_to_response('search.html', context_dict, context_instance=RequestContext(request))

