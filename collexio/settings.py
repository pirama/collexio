"""
Django settings for collexio project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

import os
from mongoengine import *

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
settings_file = os.path.abspath(__file__)

SECRET_KEY = '4t07&8(5bw=vj^le1ihuc$gxo(*2e#d4djy9_vxt)n!=1j-7z*'

DEBUG = False
TEMPLATE_DEBUG = True
DEBUG_TOOLBAR_PATCH_SETTINGS = False
ALLOWED_HOSTS = ['collex.io', 'www.collex.io', 'dev.collex.io', 'preview.collex.io']
ADMINS = (("Jimmy","jmydcpr@gmail.com"),)

if 'test' in settings_file:
    DEBUG = True
    TEMPLATE_DEBUG = True
    TWITTER_OAUTH_CALLBACK = 'http://test.collex.io/account/auth/'
    REDIS_HOST = "104.131.124.203"
    BRUNCH = "Test"

    #mongodb connection
    #connect('collexio', host='146.185.180.14', username='collexio', password='swx914922')
    db = connect(
        'collexio',
        host="mongodb://collexio:swx914922@c184.capital.0.mongolayer.com:10184,c187.capital.1.mongolayer.com:10187/collexio?replicaSet=set-54bcb12e31432f1723009e74",
        read_preference="primaryPreferred",
    )
elif 'preview' in settings_file:
    DEBUG = False
    TEMPLATE_DEBUG = True
    TWITTER_OAUTH_CALLBACK = 'http://preview.collex.io/account/auth/'
    REDIS_HOST = "104.131.124.203"
    BRUNCH = "Preview"

    #mongodb connection
    #connect('collexio', host='146.185.180.14', username='collexio', password='swx914922')
    db = connect(
        'collexio',
        host="mongodb://collexio:swx914922@c184.capital.0.mongolayer.com:10184,c187.capital.1.mongolayer.com:10187/collexio?replicaSet=set-54bcb12e31432f1723009e74",
        read_preference="primaryPreferred",
    )
elif 'worker' in settings_file:
    DEBUG = False
    TEMPLATE_DEBUG = True
    TWITTER_OAUTH_CALLBACK = 'http://preview.collex.io/account/auth/'
    REDIS_HOST = "104.131.124.203"
    BRUNCH = "Worker"

    #mongodb connection
    #connect('collexio', host='146.185.180.14', username='collexio', password='swx914922')
    db = connect(
        'collexio',
        host="mongodb://collexio:swx914922@c184.capital.0.mongolayer.com:10184,c187.capital.1.mongolayer.com:10187/collexio?replicaSet=set-54bcb12e31432f1723009e74",
        read_preference="primaryPreferred",
    )
elif 'live' in settings_file:
    DEBUG = False
    TEMPLATE_DEBUG = False
    TWITTER_OAUTH_CALLBACK = 'http://collex.io/account/auth/'
    REDIS_HOST = "104.131.124.203"
    BRUNCH = "Live"

    #mongodb connection
    #connect('collexio', host='146.185.180.14', username='collexio', password='swx914922')
    """
    db = connect(
        'collexio',
        host="mongodb://collexio:swx914922@146.185.180.14/collexio?replicaSet=wmain",
        read_preference="primaryPreferred"
    )
    """
    db = connect(
        'collexio',
        host="mongodb://collexio:swx914922@c184.capital.0.mongolayer.com:10184,c187.capital.1.mongolayer.com:10187/collexio?replicaSet=set-54bcb12e31432f1723009e74",
        read_preference="primaryPreferred",
    )
else:
    DEBUG = True
    TEMPLATE_DEBUG = True
    TWITTER_OAUTH_CALLBACK = 'http://dev.collex.io/account/auth/'
    CELERY_RESULT_BACKEND = 'mongodb://collexio:swx914922@127.0.0.1/collexio'
    REDIS_HOST = "104.131.124.203"
    STATIC_ROOT = "/var/prj/collexio/dev/collexio/static/"
    BRUNCH = "Dev"

    #mongodb connection
    #connect('collexio', host='127.0.0.1', username='collexio', password='swx914922')

    db = connect(
        'collexio',
        host="mongodb://127.0.0.1:27017",
    )


# Application definition
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'mongoengine.django.mongo_auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'debug_toolbar',
    #'debug_toolbar_mongo',
    'djcelery',
    'collection',
    'account',
    'manage',
    'import',
    'curationmac',
    'feed',
    'search'
)


MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

X_FRAME_OPTIONS = 'EXEMPT'

#A tuple of authentication backend classes (as strings) to use when attempting to authenticate a user.
AUTHENTICATION_BACKENDS = (
    'account.backends.PasswordlessAuthBackend',
)

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    #     'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    "django.core.context_processors.request",
    "collexio.context_processors.global_settings",
    "account.context_processors.side_panel_data",
)


ROOT_URLCONF = 'collexio.urls'
APPEND_SLASH = True
WSGI_APPLICATION = 'collexio.wsgi.application'


#mongo user model
MONGOENGINE_USER_DOCUMENT = 'collection.models.CUser'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.dummy',
    }
}

#Session
SESSION_ENGINE = 'mongoengine.django.sessions'
SESSION_SERIALIZER = 'mongoengine.django.sessions.BSONSerializer'


#Twitter Auth
TWITTER_CONSUMER_KEY = 'MEYX9QyPASS0tbNcHGUV1yX1Z'
TWITTER_CONSUMER_SECRET = 'wLvfK2SeT9YLW44LaBW4ZEI8KAfNaDTu2AhQ80BZuV7phDLxCo'


#For Django debug_toolbar
INTERNAL_IPS = (
    '127.0.0.1',
    '192.168.1.103',
    '192.168.0.17',
    '192.168.0.26',
    '192.168.2.46',
    #'172.20.10.4',
    '88.242.42.26',
    '172.17.20.149'
)

DEBUG_TOOLBAR_PANELS = (
    'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.redirects.RedirectsPanel',
)

DEBUG_TOOLBAR_MONGO_STACKTRACES = False


DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': True,
    'ENABLE_STACKTRACES': True,
    'SHOW_TEMPLATE_CONTEXT': True,
}

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': ('rest_framework.permissions.IsAdminUser',),
    'PAGINATE_BY': 10
}


# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True


#The URL where requests are redirected for login, especially when using the login_required() decorator.
LOGIN_URL = '/'
MEDIA_URL = '/media/'
MEDIA_ROOT = '/var/prj/collexio/media/'

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/
STATIC_URL = '/static/'


STATICFILES_DIRS = (
    #BASE_DIR+'/static/',
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    #    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

#Image Field CDN Settings
AZURE_ACCOUNT_NAME = "collexio"
AZURE_ACCOUNT_KEY = "S/WqKflVjZQMRjfwQflSaAQz1ZEobhj7N7rrd2F1vxj8RpYhIUk2EFHD3tgOboDNwaE1fgjwIch7kMZStUdG/g=="
#CDN_URL = "//collexio.blob.core.windows.net/media/"
#CDN_URL = "//collex.io/media/www/"
CDN_URL = "//dytdbxnz28lsi.cloudfront.net/"


EMAIL_HOST = "smtp.mandrillapp.com"
EMAIL_PORT = 587
EMAIL_HOST_USER = "jimmy@collex.io"
EMAIL_HOST_PASSWORD = "XiWpqvS2VI97PbDIRJzOuQ"
EMAIL_USE_TLS = False
EMAIL_FROM_ADDR = "Collexio <team@collex.io>"

BROKER_URL = "redis://%s:6379/0" % REDIS_HOST

CELERY_IMPORTS = ["import.functions",]
CELERY_MONGODB_BACKEND_SETTINGS = {
    'database': 'collexio',
    'taskmeta_collection': 'taskmeta',
}

ES_URL = 'https://pozitiftv:swx914922@aws-us-east-1-portal3.dblayer.com:10135/'