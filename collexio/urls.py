from django.conf.urls import patterns, include, url
from django.conf import settings

# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^$', 'collection.views.main', name='main'),

    url(r'^cm/', include('curationmac.urls')),
    url(r'^account/', include('account.urls')),
    url(r'^manage/', include('manage.urls')),
    url(r'^import/', include('import.urls')),

    url(r'^c/', include('collection.urls')),
    url(r'^r/c/', include('collection.rest_urls')),
    url(r'^search/', include('search.urls')),

    url(r'^twitter/callback/$', 'account.views.auth', name='callback'),
    url(r'^p/(?P<template>\w+)/$', 'account.views.pages', name='pages'),
    url(r'^email/(?P<template>\w+)/$', 'account.views.email_test', name='email'),
    
    

    url(r'^r/i/item_image_auto_upload/$', 'collection.rest.item_image_auto_upload', name='item_image_auto_upload'),
    url(r'^r/i/(?P<item_id>\w+)/$', 'collection.rest.item', name='item'),
    url(r'^r/i_html/(?P<item_id>\w+)/$', 'collection.rest.item_html', name='item_html'),
    url(r'^r/i/(?P<item_id>\w+)/edit/$', 'collection.rest.item', name='item'),
    url(r'^r/i/(?P<item_id>\w+)/accepted/$', 'collection.rest.item_accepted', name='item_accepted'),
    url(r'^r/i/(?P<item_id>\w+)/nevermind/$', 'collection.rest.item_nevermind', name='item_nevermind'),
    url(r'^r/a/user_collections/$', 'collection.rest.get_collections', name='user_collections'),
    url(r'^r/a/userlist_autocomplete/$', 'account.rest.userlist_autocomplete'),

    url(r'^r/a/follow/(?P<following_id>\w+)/$', 'account.rest.follow', name='follow'),
    url(r'^r/a/unfollow/(?P<following_id>\w+)/$', 'account.rest.unfollow', name='unfollow'),
    url(r'^r/a/notifications/$', 'account.rest.get_notifications', name='user_notifications'),

    url(r'^r/f/', include('feed.rest_urls')),


    url(r'^(?P<username>\w+)/$', 'account.views.profile', {'tab': 'public'}, name='profile'),
    url(r'^(?P<username>\w+)/followers/$', 'account.views.followers', name='profile-followers'),
    url(r'^(?P<username>\w+)/following/$', 'account.views.following_users', name='profile-following'),
    url(r'^(?P<username>\w+)/private/$', 'account.views.profile', {'tab': 'private'}, name='private-collections'),
    url(r'^(?P<username>\w+)/unlisted/$', 'account.views.profile', {'tab': 'unlisted'}, name='unlisted-collections'),
    url(r'^(?P<username>\w+)/contributions/$', 'account.views.profile', {'tab': 'collaborations'}, name='account-collaborations'),
    #url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += patterns(
        '',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )
