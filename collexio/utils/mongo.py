

def mongo_to_dict(obj):
    from mongoengine import *

    """ Turn a mongoengine object into a jsonible python object
    """

    return_data = []

    if isinstance(obj, Document):
        return_data.append(("id", str(obj.id)))

    for field_name in obj._fields:
        data = obj._data[field_name]

        if isinstance(obj._fields[field_name], DateTimeField):
            return_data.append((field_name, str(data.isoformat())))

        elif isinstance(obj._fields[field_name], StringField):
            return_data.append((field_name, str(data)))

        elif isinstance(obj._fields[field_name], FloatField):
            return_data.append((field_name, float(data)))

        elif isinstance(obj._fields[field_name], IntField):
            return_data.append((field_name, int(data)))

        elif isinstance(obj._fields[field_name], ListField):
            return_data.append((field_name, data))

        elif isinstance(obj._fields[field_name], EmbeddedDocumentField):
            return_data.append((field_name, mongo_to_dict(data)))

        elif isinstance(obj._fields[field_name], ObjectIdField):
            return_data.append((field_name, str(data)))

        else:
            print type(obj._fields[field_name])

    return dict(return_data)
