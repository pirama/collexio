import json
from django.http import HttpResponse

def json_request(data):
    return HttpResponse(
        json.dumps(data),
        content_type='application/javascript; charset=utf8'
    )

def json_response(data):
    return HttpResponse(
        json.dumps(data),
        content_type='application/javascript; charset=utf8'
    )

def test_response(data):
    return HttpResponse()