from django import forms
from models import *

class BufferForm(forms.Form):
    name = forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control'}))
    collection = forms.ChoiceField(required=True,widget=forms.Select(attrs={'class': 'form-control'}))

    def __init__(self, *args, **kwargs):
        self.instance = kwargs.pop('instance', None)
        super(BufferForm, self).__init__(*args, **kwargs)
        self.fields['collection'].choices = [(c.pk, c.title) for c in Collection.objects.all().order_by("title")]

        if self.instance:
          self.fields['name'].initial = self.instance.name
          self.fields['collection'].initial = self.instance.collection

    def save(self, commit=True):
        buff = self.instance if self.instance else Buff()
        buff.name = self.cleaned_data['name']
        buff.collection = Collection.objects.get(id=self.cleaned_data['collection'])

        if commit:
            try:
                buff.save()
            except Exception, e:
                raise e

        return buff