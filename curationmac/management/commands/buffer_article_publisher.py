import logging
from django.core.management.base import BaseCommand, CommandError
from django.db.models.signals import post_save
from curationmac.utils import random_date
from curationmac.models import Buff, BufferArticles
from collection.models import get_url_data
from collection.views import add_edit_item


def select_articles(buff):
    first_article = BufferArticles.objects(buff=buff, is_used__ne=True).order_by("id")[:1]
    end = first_article[0].article.date
    logging.debug(end)

    last_article = BufferArticles.objects(buff=buff, is_used__ne=True).order_by("-id")[:1]
    start = last_article[0].article.date
    logging.debug(start)

    article_list = []
    for i in range(1, 3):
        r_date = random_date(start, end)
        logging.debug(r_date)
        buffer_article = BufferArticles.objects(is_used__ne=True, buff=buff, date__lte=r_date)[:1]
        if buffer_article:
            article_list.append(buffer_article[0])

    return article_list


def post_to_collection(item, collection):
    item_data = get_url_data(item.url)
    item = add_edit_item(item_data, collection=collection)



def loop():
    logger = logging.getLogger('BufferArticlePublisher')
    buffer_list = Buff.objects.all()
    for buff in buffer_list:
        logging.debug(buff.name)
        buff_articles = select_articles(buff)
        for buff_article in buff_articles:
            post_to_collection(buff_article.article, buff.collection)
            logger.info(buff_article.article.title + "published  to " + buff.collection.title)


class Command(BaseCommand):
    def handle(self, *args, **options):
        loop()
