import time
from curationmac.utils import process_rss
from django.core.management.base import BaseCommand

class Command(BaseCommand):
    def handle(self, *args, **options):
        while 1:
            try:
                process_rss()
                time.sleep(180)
            except Exception, e:
                logging.error(e)
