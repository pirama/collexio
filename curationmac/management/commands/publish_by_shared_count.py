import logging
import time
from datetime import datetime, timedelta
from django.core.management.base import BaseCommand
from django.db.models.signals import post_save
from curationmac.models import ArticleSource, Article
from curationmac.utils import social_shared_count
from collection.models import Collection
from collection.models import get_url_data
from collection.views import add_edit_item


class Command(BaseCommand):
    def update_shared_count(self):
        # select tech sources
        sources = ArticleSource.objects(category="tech")
        article_list = []
        one_hour_before = datetime.utcnow() - timedelta(hours=1)
        two_hour_before = datetime.utcnow() - timedelta(hours=2)
        logging.debug(one_hour_before)
        articles = Article.objects(source__in=sources, total_shared_count__exists=False, date__lte=one_hour_before, date__gte=two_hour_before)

        for a in articles:
            article_list.append(a)

        logging.debug(article_list)

        for a in article_list:
            logging.debug(a.url)
            a['shared_count'] = social_shared_count(a['url'])
            a['total_shared_count'] = a['shared_count']['total']
            a.save()

        article = Article.objects(
            source__in=sources,
            date__lte=one_hour_before,
            is_shared=False
        ).order_by("-total_shared_count")[:1]

        a = article[0]
        logging.debug(a.total_shared_count)
        collection = Collection.objects.get(slug="important-news-on-startups-and-technology-1416388275")
        self.post_to_collection(a, collection)
        a.is_shared = True
        a.save()
        logging.debug("Article "+a.url+" shared on Tech collection")

    def post_to_collection(self, item, collection):
        item_data = get_url_data(item.url)
        item = add_edit_item(item_data, collection=collection)
        post_save.send(sender=item)


    def handle(self, *args, **options):
        while True:
            self.update_shared_count()
            time.sleep(3600)
