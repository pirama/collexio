from mongoengine import *
from collection.models import Collection


class ArticleSource(Document):
    url = StringField()
    category = StringField()
    etag = StringField()
    last_modified = StringField()


class Article(Document):
    sync_id = StringField()
    title = StringField()
    body = StringField()
    url = StringField()
    date = DateTimeField()
    category = StringField()
    source = ReferenceField(ArticleSource, reverse_delete_rule=CASCADE, required=False)
    collections = ListField(ReferenceField(Collection, reverse_delete_rule=CASCADE), required=False)
    shared_count = DictField()
    total_shared_count = IntField()
    is_shared = BooleanField(default=False)

    @property
    def buffers(self):
        buffers = BufferArticles.objects(article=self)
        buffer_list = []
        for buffer_article in buffers:
            buffer_list.append(buffer_article.buff)

        return buffer_list


class Buff(Document):
    name = StringField()
    collection = ReferenceField(
        Collection,
        reverse_delete_rule=CASCADE,
        unique=True
    )


class BufferArticles(Document):
    buff = ReferenceField(Buff, reverse_delete_rule=CASCADE)
    article = ReferenceField(Article, reverse_delete_rule=CASCADE, unique_with="buff")
    is_used = BooleanField(required=True, default=False)
    date = DateTimeField()

    def save(self, *args, **kwargs):
        if not self.date:
            self.date = self.article.date

        return super(BufferArticles, self).save(*args, **kwargs)
