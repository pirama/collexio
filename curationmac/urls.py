from django.conf.urls import patterns, url, include

urlpatterns = patterns(
    '',
    url(r'^buffers/create/', 'curationmac.views.create_buffer', name="cm_create_buffer"),

    url(r'^buffers/(?P<buffer_id>[-\w]+)/add/(?P<article_id>[-\w]+)/', 'curationmac.views.buffer_add_article', name="cm_buffer_add_article"),
    url(r'^buffers/(?P<buffer_id>[-\w]+)/remove/(?P<article_id>[-\w]+)/', 'curationmac.views.buffer_remove_article', name="cm_buffer_remove_article"),
    url(r'^buffers/(?P<buffer_id>[-\w]+)/', 'curationmac.views.buffer_articles', name="cm_buffer_articles"),
    url(r'^buffers/', 'curationmac.views.buffer_list', name="cm_buffer_list"),

    url(r'^cat/(?P<category>[-\w]+)/', 'curationmac.views.index'),
    url(r'^', 'curationmac.views.index'),
)
