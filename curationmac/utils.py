import logging
import feedparser
import requests
import urllib
import json
from random import randint
from datetime import timedelta
from models import ArticleSource, Article
from libs.common import remove_query_params


def process_rss():
    sources = ArticleSource.objects.all()

    for source in sources:
        logging.debug(source.url)

        if 'etag' in source:
            data = feedparser.parse(source.url, etag=source.etag)
        elif 'last_modified' in source:
            data = feedparser.parse(source.url, modified=source.last_modified)
        else:
            data = feedparser.parse(source.url)

        items = data['entries']

        for item in items:
            process_item(item, source)

        if 'etag' in data:
            source.etag = data.etag
        elif 'modified' in data:
            source.last_modified = data.modified

        source.save()


def process_item(item, source):
    if "feedburner_origlink" in item:
        item['link'] = item['feedburner_origlink']
    if 'id' not in item:
        try:
            item['id'] = str(source.pk)+":"+item['link']
        except Exception:
            logging.debug(str(item['title'])+"NOT HAS LINK")
            return

    save_article(item, source)


def save_article(item, source):
    try:
        content = Article.objects.get(sync_id=item['id'])
        # if the content is already in db. return false. we already process this content.
        return False
    except:
        r = requests.head(item['link'], allow_redirects=True)
        item['link'] = remove_query_params(r.url)

        try:
            content = Article(
                sync_id=item['id'],
                title=item['title'],
                url=item['link'],
                source=source,
            )

            if 'summary' in item:
                content.body = item['summary']

            if 'published' in item:
                content.date = item['published']

            content.save()
        except Exception, e:
            logging.debug(e)
            return False

    return content


def random_date(start, end):
    """
    This function will return a random datetime between two datetime
    objects.
    """
    return start + timedelta(seconds=randint(0, int((end - start).total_seconds())))


def social_shared_count(url):
    api_domain = "https://free.sharedcount.com/url"
    api_key = "92e293a5551b6724c0eaa375489346156291f211"
    r = requests.get(api_domain+"?apikey="+api_key+"&url="+url)
    sc = json.loads(r.content)
    sc['total'] = 0
    if "Twitter" in sc:
        sc['total'] += int(sc['Twitter'])
    if "Facebook" in sc:
        if "share_count" in sc['Facebook']:
            sc['total'] += int(sc['Facebook']['share_count'])
    if sc['GooglePlusOne'] is not None:
        sc['total'] += int(sc['GooglePlusOne'])
    if "LinkedIn" in sc:
        sc['total'] += int(sc['LinkedIn'])
    if "Pinterest" in sc:
        sc['total'] += int(sc['Pinterest'])
    logging.debug(sc)
    return sc
