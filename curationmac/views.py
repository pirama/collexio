import logging
import json
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from mongoengine import *
from models import ArticleSource, Article, Buff, BufferArticles
from forms import BufferForm
from django.contrib import messages


def index(request, category=None):
    category_list = {
        "digitalmarketing": "Digital Marketing",
        "tech": "Technology & Startups"
    }

    if not category:
        category = "digitalmarketing"

    rsssources = ArticleSource.objects(category=category)
    articles = Article.objects(source__in=rsssources).order_by("-date")
    buffer_list = Buff.objects.all()
    ctx = {'articles': articles, 'buffer_list': buffer_list, "tab": "articles", "category_list": category_list, "category": category}
    return render_to_response('cm_index.html', ctx, context_instance=RequestContext(request))


def buffer_list(request):
    buffer_list = Buff.objects.all()
    ctx = {'buffer_list': buffer_list, "tab": "buffers"}
    return render_to_response('cm_buffer_list.html', ctx, context_instance=RequestContext(request))


def buffer_articles(request, buffer_id):
    buff = Buff.objects.get(pk=buffer_id)
    articles = BufferArticles.objects(buff=buff)
    ctx = {'articles': articles, "buffer": buff, "tab": "buffers"}
    return render_to_response('cm_buffer_articles.html', ctx, context_instance=RequestContext(request))


def buffer_add_article(request, buffer_id, article_id):
    ctx = {}
    b = BufferArticles(
        buff=Buff.objects.get(id=buffer_id),
        article=Article.objects.get(id=article_id)
    )
    try:
        b.save()
        ctx['status'] = "ok"
        ctx['message'] = "Saved"
    except Exception, e:
        ctx['status'] = "failed"
        ctx['message'] = str(e)

    return HttpResponse(
        json.dumps(ctx),
        content_type='application/javascript; charset=utf8'
    )


def buffer_remove_article(request, buffer_id, article_id):
    ctx = {}
    try:
        buff = Buff.objects.get(pk=buffer_id)
        article = Article.objects.get(pk=article_id)
        a = BufferArticles.objects.get(buff=buff, article=article)
        a.delete()
        ctx['status'] = "ok"
        ctx['message'] = "Removed"
    except Exception, e:
        ctx['status'] = "failed"
        ctx['message'] = str(e)

    return HttpResponse(
        json.dumps(ctx),
        content_type='application/javascript; charset=utf8'
    )


def collection_add_article(request, collection_id, article_id):
    pass


def create_buffer(request):
    if request.method == "POST":
        form = BufferForm(request.POST)
        if form.is_valid():
            try:
                form.save()
                messages.add_message(request, messages.SUCCESS, "Buffer Created")
                return HttpResponseRedirect(reverse('cm_buffer_list'))
            except NotUniqueError, e:
                logging.debug(e)
                messages.add_message(request, messages.ERROR, e.message)
    else:
        form = BufferForm()

    ctx = {'form': form}
    return render_to_response('cm_buffer_form.html', ctx, context_instance=RequestContext(request))


def edit_buffer(request):
    pass
