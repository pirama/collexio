from fabric.api import *
from fabric.operations import put


@task
def github_keys():
    put('/root/.ssh/github', '/root/.ssh/id_rsa', use_sudo=True)
    put('/root/.ssh/github.pub', '/root/.ssh/id_rsa.pub', use_sudo=True)
    sudo('chmod 400 /root/.ssh/id_rsa')


@task
def pull():
    with cd('/var/sites/test/pozitiftv'):
        sudo('git pull origin master')


@task
def list():
    local("cat ~/.ssh/config")


@task
def pstree():
    run("pstree")


@task
def cmd(cmd):
    run(cmd)


@task
def service_restart(service):
    sudo("service %s restart" % service)


@task
def service_stop(service):
    sudo("service %s stop" % service)
