from fabric.api import *
from fabric.contrib import files
base_dir = "/var/prj/"

env.roledefs = {'app': ['app1-ams.w', 'app1-nyc2.w']}
env.user = "root"


@task
def start(project_name):

    create_folders(project_name)

    with cd(base_dir+project_name+"/test"):
        run("django-admin.py startproject "+project_name)
        run("git init")
        run("git add .")
        run("git commit -a -m 'first commit'")
        run("git remote add origin git@github.com:cemevren/"+project_name+".git")
        run("git pull origin master")
        run("git push origin master")


@task
def create_folders(project_name):
    run("mkdir "+base_dir+project_name)
    run("mkdir "+base_dir+project_name+"/live")
    run("mkdir "+base_dir+project_name+"/test")
    run("mkdir "+base_dir+project_name+"/conf")
    run("mkdir "+base_dir+project_name+"/media")


@task
def rm(project_name):
    with cd(base_dir):
        run("rm -R "+project_name)


@hosts('app2-nyc2.c')
@task
def deploy(project_name="collexio"):
    with cd(base_dir+project_name+"/deploy/"):
        if files.exists(project_name):
            sudo("rm -R "+project_name)

        sudo("git clone https://github.com/cemevren/"+project_name+".git")


@hosts('app2-nyc2.c')
@task
def make(project_name, domain, branch):
    deploy_dir = base_dir+project_name+"/deploy/"+project_name
    domain_dir = base_dir+project_name+"/"+domain+"/"+project_name

    if files.exists(domain_dir):
        if files.exists(domain_dir+"_old"):
            sudo("rm -R "+domain_dir+"_old")

        sudo("mv "+domain_dir+" "+domain_dir+"_old")

    sudo("cp -R "+deploy_dir+" "+domain_dir)

    with cd(domain_dir):
        sudo("git checkout "+branch)

    #sudo("service uwsgi reload")


@hosts('app1-nyc2.c')
@task
def make_live(project_name):
    test_dir = base_dir+"/test/"+project_name
    live_dir = base_dir+"/live/"+project_name

    if files.exists(live_dir):
        sudo("rm -R "+live_dir+"_old")
        sudo("mv "+live_dir+" "+live_dir+"_old")

    sudo("cp -R "+test_dir+" "+live_dir)
    sudo("service uwsgi reload")
