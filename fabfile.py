from fabric.api import *
from fab import host, prj

env.use_ssh_config = True


@task
def install_newrelic_server():
    run("wget -O /etc/apt/sources.list.d/newrelic.list http://download.newrelic.com/debian/newrelic.list")
    run("apt-key adv --keyserver hkp://subkeys.pgp.net --recv-keys 548C16BF")
    run("apt-get update")
    run("apt-get install newrelic-sysmond")
    run("nrsysmond-config --set license_key=3eba1e6d64f4092455ee6b8e10a3c04f6e035ee2")
    run("/etc/init.d/newrelic-sysmond start")


@task
def install_newrelic_python(project):

    project_dir = "/var/sites/%s" % project

    run("pip install newrelic")
    run("cd %s/conf" % project_dir)
    run("newrelic-admin generate-config 3eba1e6d64f4092455ee6b8e10a3c04f6e035ee2 newrelic.ini")
    #edit config file


@task
def install_wowza():
    run("wget http://92.45.113.3/WowzaMediaServer.deb.bin")
    run("chmod +x WowzaMediaServer.deb.bin")
    run("./WowzaMediaServer.deb.bin")
    run("cd /usr/local/WowzaMediaServer/bin")
    run("./startup.sh")
