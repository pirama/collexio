import logging, time
from datetime import timedelta
from mongoengine import *
from feed.models import CollectionActivityFeed
from account.models import CUser
from collection.models import Collection, Item
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    def handle(self, *args, **options):
        # delete all newpost activity
        r = CollectionActivityFeed.objects(activity="newpost")
        r.delete()

        # walk through items and populate activity rows
        items = Item.objects.all()
        for item in items:
            try:
                item.collection.owner
            except:
                continue

            try:
                logging.debug(item.pk)
                delta = timedelta(minutes=15)
                logging.debug(delta)
                f = CollectionActivityFeed.objects.get(
                    user=item.collection.owner,
                    collection=item.collection,
                    created__gt=item.added-delta,
                    activity="newpost"
                )

            except:
                f = CollectionActivityFeed(
                    user=item.collection.owner,
                    collection=item.collection,
                    created=item.added,
                    activity="newpost"
                )

            if item.collection.is_private or item.collection.is_unlisted:
                f.is_hidden = True
            else:
                f.is_hidden = False

            f.item_list.append(item)
            f.save()
            # time.sleep(1)
