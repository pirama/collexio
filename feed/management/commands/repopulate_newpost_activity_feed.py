import logging, time
from datetime import timedelta
from mongoengine import *
from feed.models import CollectionActivityFeed
from account.models import CUser
from collection.models import Collection, Item
from django.core.management.base import BaseCommand, CommandError



class Command(BaseCommand):
    def handle(self, *args, **options):
        #delete all newpost activity
        r = CollectionActivityFeed.objects(activity="newpost")
        r.delete()

        #walk through items and populate activity rows
        items = Item.objects.all()
        for item in items:
            if item.collection.owner:
                try:
                    logging.debug(item.pk)
                    delta = timedelta(hours=2)
                    logging.debug(delta)
                    f = CollectionActivityFeed.objects.get(
                        user=item.collection.owner,
                        collection=item.collection,
                        created__gt=item.added-delta,
                        activity="newpost"
                    )

                except:
                    f = CollectionActivityFeed(
                        user=item.collection.owner,
                        collection=item.collection,
                        created=item.added,
                        activity="newpost"
                    )

                f.item_list.append(item)
                f.save()
                time.sleep(1)

