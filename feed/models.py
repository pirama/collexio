from mongoengine import *
from account.models import CUser
from collection.models import Collection, Item


class CollectionActivityFeed(Document):

    created = DateTimeField()
    user = ReferenceField(CUser, reverse_delete_rule=CASCADE)
    collection = ReferenceField(Collection, reverse_delete_rule=CASCADE)
    item = ReferenceField(Item, reverse_delete_rule=CASCADE)
    item_list = ListField(ReferenceField(Item, reverse_delete_rule=CASCADE))
    activity = StringField(required=True)
    is_hidden = BooleanField(required=False)
