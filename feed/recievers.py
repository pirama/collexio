# -*- coding: utf-8 -*-
from datetime import timedelta, datetime
import logging


def activity_autosave_handler(sender, document, **kwargs):
    from collection.models import Item, Subscriptions, Recommendations
    from models import CollectionActivityFeed

    if sender == Item:
        activity = "newpost"
        item = document
        collection = item.collection
        user = item.collection.owner
    elif sender == Subscriptions:
        activity = "subscribed"
        collection = document.collection
        user = collection.owner
    elif sender == Recommendations:
        activity = "recommended"
        collection = document.collection
        user = collection.owner

    # check record is exists
    if sender == Item:
        try:
            delta = timedelta(minutes=15)

            f = CollectionActivityFeed.objects.get(
                user=user,
                collection=collection,
                created__gt=item.added-delta,
                activity=activity
            )

        except:
            f = CollectionActivityFeed(
                user=user,
                collection=collection,
                created=item.added,
                activity=activity
            )

        if item.collection.is_private or item.collection.is_unlisted:
            f.is_hidden = True
        else:
            f.is_hidden = False

        f.item_list.append(item)
        f.save()
    else:
        try:
            CollectionActivityFeed.objects.get(
                user=user,
                collection=collection,
                activity=activity
            )
        except:
            CollectionActivityFeed(
                user=user,
                collection=collection,
                activity=activity,
                created=datetime.now
            ).save()


def activity_delete_handler(sender, document, **kwargs):
    from collection.models import Item, Subscriptions, Recommendations
    from models import CollectionActivityFeed

    if sender == Item:
        activity = "newpost"
        item = document
        collection = item.collection
        user = item.collection.owner
    elif sender == Subscriptions:
        activity = "subscribed"
        collection = document.collection
        user = collection.owner
    elif sender == Recommendations:
        activity = "recommended"
        collection = document.collection
        user = collection.owner


    if sender == Item:
        r = CollectionActivityFeed.objects.get(
                user=user,
                collection=collection,
                activity=activity,
                item_list__in=item
            )

        r.item_list.remove(item)
        r.save()

    else:
        try:
            a = CollectionActivityFeed.objects.get(
                user=user,
                collection=collection,
                activity=activity,
            )

            a.delete()
        except:
            pass


def feed_privacy_update_handler(sender, document, **kwargs):
    from models import CollectionActivityFeed

    collection = document
    activity = CollectionActivityFeed.objects(collection=collection)[0:1]

    if activity:
        activity = activity[0]

        if (collection.is_private or collection.is_unlisted) and not activity.is_hidden:

            logging.debug("activity is not hidden")
            CollectionActivityFeed.objects(collection=collection).update(set__is_hidden=True)

        elif (not collection.is_private and not collection.is_unlisted) and activity.is_hidden:

            logging.debug("activity is hidden")
            CollectionActivityFeed.objects(collection=collection).update(set__is_hidden=False)
