# -*- coding: utf-8 -*-
import logging
import json
import uuid, imghdr, re, requests, urlparse, cStringIO, logging, html5lib
from django.shortcuts import render_to_response
from django.template.loader import get_template
from django.template import Context

from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.template import RequestContext

from models import *
import feed.utils as feed_utils


def get_feed_activities(request):
    since_id = request.GET.get('since_id', 0)
    activities = feed_utils.get_feed(user=request.user, since_id=since_id, count=10)

    html_content = ""
    for activity in activities:
        if activity.activity == "newpost":
            htmly = get_template("feed_item_layout.html")
            ctx = {
                'activity': activity,
                'collection': activity.collection,
                'user': request.user
            }
        else:
            htmly = get_template("feed_collection_layout.html")
            ctx = {
                'activity': activity,
                'collection': activity.collection,
                'user': request.user
            }

        d = Context(ctx)
        html_content += htmly.render(d)

    data = {}
    data['html'] = json.dumps(html_content)

    """
    if item_list.has_next():
        data['p']['next_page_number'] = item_list.next_page_number()
    """

    return HttpResponse(
        json.dumps(data),
        content_type='application/javascript; charset=utf8'
    )
