import logging
from feed.models import CollectionActivityFeed
from mongoengine import Q
from django.core.paginator import Paginator
from bson.objectid import ObjectId


def get_feed(user=None, since_id="FFFFFFFFFFFFFFFFFFFFFFFF", count=0):
    from collection.models import Collection
    following = user.following
    following.append(user)
    subscriptions = user.get_subscriptions()
    following_users_collections = Collection.objects(owner__in=following, is_private__ne=True, is_unlisted__ne=True).order_by('-item_added')

    f_list = list(following_users_collections.all())

    result_list = subscriptions + list(set(f_list) - set(subscriptions))

    activities = CollectionActivityFeed.objects(
        (Q(activity="recommended") & Q(user__in=following) & Q(is_hidden__ne=True)) |
        (Q(activity="newpost") & Q(collection__in=result_list)), id__lt=ObjectId(since_id)
    ).order_by('-id')[0:count]

    """
    activity_list = []
    for row in p:
        logging.debug(row.activity)
        if row.activity == "newpost":
            row['item'] = row.item_list[-1]

        activity_list.append(row)
    """

    return activities
