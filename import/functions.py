# -*- coding: utf-8 -*-
import os
import logging
import uuid
from django.conf import settings
from bs4 import BeautifulSoup
from collection.models import *
from collection.views import add_edit_item
from collexio.celeryapp import app


@app.task
def run_import(export_file, user):
    file_path = handle_upload(export_file)
    data = parse_urllist_file(file_path)
    create_collections(data, user)


def handle_upload(file_object):
    ext = os.path.splitext(file_object.name)[1]
    file_name = uuid.uuid4().hex
    base = MEDIA_ROOT
    part = 'uploads/export_files/{}{}'.format(file_name, ext)
    file_name = file_name+ext
    file_path = base+part
    with open(file_path, 'wb+') as destination:
        for chunk in file_object.chunks():
            destination.write(chunk)

    return file_path


def parse_urllist_file(file_path):
    soup = BeautifulSoup(open(file_path), "html5lib")
    lists = soup.ul.find_all("li", recursive=False)
    url_lists = []
    for _list in lists:
        url_list = {}

        url_list['title'] = _list.h2.string
        if _list.p:
            url_list['description'] = _list.p.string
        url_list['created'] = _list.time.string

        url_lists.append(url_list)

        items_array = []
        items = _list.ul.find_all("li", recursive=False)

        for item in items:
            url_list_item = {}
            url_list_item['title'] = item.p.a.string
            url_list_item['url'] = item.p.a['href']

            item.p.a.extract()
            description = item.p.string
            description = description.replace("\n\n", "")
            description = description.replace(u"\u2014", "")

            url_list_item['description'] = description.strip()
            url_list_item['added'] = str(item.time.string)

            items_array.append(url_list_item)

        url_list['items'] = items_array

    return url_lists


def create_collections(url_lists, user):
    for _list in url_lists:
        collection = Collection(
            title=_list['title'],
            description=_list['description'],
            owner=user
        )

        collection = collection.save()

        for item in _list['items']:
            url = item['url']
            item_data = get_url_data(url)
            logging.debug(item_data)
            add_edit_item(item_data, collection=collection)

    from collection.models import ImportCompleteNotification, CUser
    collexio = CUser.objects.get(pk="53cd1041e7798979b3f1eb49")

    ImportCompleteNotification(
        to=user,
        nt_from=collexio,
        import_source="urli.st"
    ).send_nt()
