# -*- coding: utf-8 -*-
import json
from functions import *
from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.template import RequestContext
from django.contrib.auth.decorators import login_required


@login_required(login_url=settings.LOGIN_URL)
def urllist_import(request):

    if request.method == "POST":
        if 'export_file' in request.FILES:
            if request.FILES['export_file']:

                run_import.delay(request.FILES['export_file'], request.user)
                #run_import(request.FILES['export_file'], request.user)

                """
                return HttpResponse(
                    json.dumps(data),
                    content_type='application/javascript; charset=utf8'
                )
                """
                ctx = {}
                return render_to_response('import_urllist.html', ctx, context_instance=RequestContext(request))

    if request.method == "GET":
        ctx = {}
        return render_to_response('import_urllist.html', ctx, context_instance=RequestContext(request))
