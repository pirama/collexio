# -*- coding: utf-8 -*-
#import logging
import json
import requests, urlparse, re


class Card(object):
    card = {}
    special_cards = {
        'vineVideo': {
            'type': 'video',
            'pattern': '\S*vine.co/v',
            'func': 'vine_embed'
        },
        'youtubeVideo': {
            'type': 'video',
            'pattern': '\S*youtube.com/watch',
        },
        'vimeoVideo': {
            'type': 'video',
            'pattern': '\S*vimeo\.com/\d+'
        },
        'twitterTweet': {
            'type': 'video',
            'pattern': '\S*twitter\.com/\w+/status/\d+'
        },
        'amazonProduct': {
            'type': 'rich',
            'pattern': '\S*amazon\.com/gp/product/\w+',
            'func': 'embedly'
        }
    }

    oembed_providers = ['flickr.com']

    def __init__(self, item):
        self.url = item.url

    @property
    def label(self):
        for k, v in self.card_labels:
            if re.match(v['pattern'], self.url):
                return k
