from urlparse import urlparse


def remove_query_params(url):
    r = urlparse(url)
    url = r.geturl()
    url = url.replace(r.query, "")
    url = url.replace("?", "")
    return url
