# -*- coding: utf-8 -*-
import uuid, imghdr, re, requests, urlparse, cStringIO, logging
import boto
from boto.s3.key import Key
from shutil import copyfile
from collexio.settings import *
from PIL import Image, ImageOps
#from azure.storage import *



class ImageDict(DictField):
    def __init__(self, thumb_tuple=None, *args, **kwargs):
        logging.debug('init')
        if len(thumb_tuple) <= 0 or not isinstance(thumb_tuple, tuple):
            raise ValueError(
                '''You should provide a thumb dimensions as python tuble.
                Ex: ([640,360,"large"],[320,140,"small"],[80,80,"thumb"])''')
        else:
            self.thumb_tuple = thumb_tuple

        return super(ImageDict, self).__init__(*args, **kwargs)

    def __set__(self, instance, value):
        """ image'ın local path'i veya post file objesi value olarak verilebilir"""
        image_dict = {}
        uploaded_image_path = ""
        if instance._initialised:
            try:
                if (self.name not in instance._data or instance._data[self.name] != value):
                    instance._mark_as_changed(self.name)
            except:
                instance._mark_as_changed(self.name)

        if isinstance(value, dict):
            instance._data[self.name] = value
            return

        if value is None:
            instance._data[self.name] = value
            return

        # get uploaded or remote file to local directory
        if isinstance(value, str) or isinstance(value, unicode):
            # check value is a image url
            try:
                parsed = urlparse.urlparse(value)
                if not parsed.scheme:
                    uploaded_image_path = MEDIA_ROOT+value
            except Exception, e:
                # if fails value is a local file path
                uploaded_image_path = value

            logging.debug(value)
            logging.debug(parsed)
            logging.debug(uploaded_image_path)

            if parsed:
                if parsed.scheme:
                    try:
                        headers = {
                            "User-Agent": "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"
                        }
                        response = requests.get(value, headers=headers, verify=False)
                        img = Image.open(cStringIO.StringIO(response.content))
                    except Exception, e:
                        raise ValueError(
                            "Something goes wrong. I can't see the image. -Open error."+str(e)
                        )

                    try:
                        file_name_array = os.path.basename(urlparse.urlsplit(value).path)
                        ext = os.path.splitext(file_name_array)[1]
                        if not ext:
                            ext = ".jpg"

                        file_name = str(uuid.uuid4().hex)+str(ext)
                        uploads_dir = MEDIA_ROOT+'uploads/images/'
                        uploaded_image_path = uploads_dir+file_name
                        img.save(uploaded_image_path)
                    except Exception, e:
                        raise ValueError(
                            "Something goes wrong. I can't see the image. -Save error."+str(e)
                        )
        else:
            upload_file_object = value
            uploaded_image_path = self.handle_upload(upload_file_object)

        # validate image
        self.is_valid(uploaded_image_path)

        # get image_dict for original
        image_dict.update({
            'original': self.get_image_dict(uploaded_image_path)
        })

        # upload original file to azure
        # self.send_to_azure(uploaded_image_path)
        self.send_to_s3(uploaded_image_path)

        # create thumbnail for every size
        for size in self.thumb_tuple:
            if len(size) < 3:
                raise ValueError("thumb tuple must have 3 values: width,height,name")
            try:
                thumb_path = self.create_thumbnail(uploaded_image_path, size[0], size[1])
                thumb_dict = self.get_image_dict(thumb_path)
                image_dict[size[2]] = thumb_dict
                self.send_to_s3(thumb_dict['path'])
            except Exception, e:
                raise ValueError("Something goes wrong. I can't see the image. Image file may be corrupted "+str(e))

        # put image dict to field
        instance._data[self.name] = image_dict

    def __get__(self, instance, owner):
        if instance is None:
            # Document class being used rather than a document object
            return self

        # Get value from document instance if available
        value = instance._data.get(self.name)
        return value

    def get_image(self, size_str):
        value = instance._data.get(self.name)
        return CDN_URL+value[size_str]['file_name']

    @staticmethod
    def handle_upload(file_object):
        """ Upload image and return the path"""
        ext = os.path.splitext(file_object.name)[1]
        if not ext:
            ext = ".jpg"
        file_name = uuid.uuid4().hex
        base = MEDIA_ROOT
        part = 'uploads/images/{}{}'.format(file_name, ext)
        file_name = file_name+ext
        file_path = base+part
        with open(file_path, 'wb+') as destination:
            for chunk in file_object.chunks():
                destination.write(chunk)

        return file_path

    @staticmethod
    def create_thumbnail(image_path, width, height):
        '''
        Builds a thumbnail by cropping out a maximal region from the center of the original with
        the same aspect ratio as the target size, and then resizing. The result is a thumbnail which is
        always EXACTLY the requested size and with no aspect ratio distortion (although two edges, either
        top/bottom or left/right depending whether the image is too tall or too wide, may be trimmed off.)

        Returns thumbnail file_path
        '''

        uploads_dir = MEDIA_ROOT+'uploads/images/'

        img = Image.open(image_path)

        # gelen ölçülerden biri sıfır ise sadece verilen ölçüye göre resize ediyoruz. Değilse resize&crop
        if width == 0 or height == 0:
            ratio = float(img.size[0])/float(img.size[1])
            logging.debug(img.size[0])
            logging.debug(img.size[1])
            logging.debug(ratio)
            logging.debug(width)
            logging.debug(height)

            if width == 0:
                width = ratio*height
            else:
                height = width/ratio

            logging.debug(width)
            logging.debug(height)
            size = (int(width), int(height))
            img = img.resize(size,Image.ANTIALIAS)
            logging.debug(img)
        else:
            img = ImageOps.fit(img, [width, height], Image.ANTIALIAS)

        # split filename and extention
        file_name = os.path.basename(image_path)
        base_name = os.path.splitext(file_name)[0]
        ext = os.path.splitext(file_name)[1]
        if not ext:
            ext=".jpg"

        w = img.size[0]
        h = img.size[1]
        size_str = str(w)+"x"+str(h)

        new_name = base_name+"_"+size_str+ext
        new_file_path = uploads_dir+"/"+new_name
        # img = ImageOps.autocontrast(img)
        img.save(new_file_path, quality=85, subsampling=0)

        return new_file_path

    """
    @staticmethod
    def send_to_azure(file_path):
        blob_service = BlobService(account_name=AZURE_ACCOUNT_NAME, account_key=AZURE_ACCOUNT_KEY)
        file_name = os.path.basename(file_path)
        myblob = open(file_path, 'r').read()
        blob_service.put_blob('media', file_name, myblob, x_ms_blob_type='BlockBlob')
    """

    @staticmethod
    def send_to_mediadir(file_path):
        try:
            file_name = os.path.basename(file_path)
            print file_name
            copyfile(file_path, "/var/prj/collexio/media/www/"+file_name)
        except Exception, e:
            raise e

    @staticmethod
    def send_to_s3(file_path):
        AWS_ACCESS_KEY_ID = "AKIAJ5OLQHKPRRNSKZYQ"
        AWS_SECRET_ACCESS_KEY = "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"

        conn = boto.connect_s3(AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY)
        bucket = conn.get_bucket('collexio-media')

        file_name = os.path.basename(file_path)

        k = Key(bucket)
        k.key = file_name
        k.set_contents_from_filename(file_path)
        """
        k.metadata.update({
            'Cache-Control': 'max-age=2592000'
        })
        """




    @staticmethod
    def get_image_dict(file_path):
        """ create file on local directory (uploads/images) for an PIL image object and return file dict  """

        img = Image.open(file_path)
        basename = os.path.basename(file_path)
        w = img.size[0]
        h = img.size[1]

        img_dict = {
            "path": file_path,
            "filename": basename,
            "size_str": str(w)+"x"+str(h),
            "w": w,
            "h": h
        }
        return img_dict

    @staticmethod
    def is_valid(file_path):
        file_size = os.path.getsize(file_path)
        if file_size > 5120000:
            raise ValueError("Image file size must be smaller than 5mb.")

        """
        file_type = imghdr.what(file_path)
        if file_type != "jpeg" and file_type != "jpg" and file_type != "png" and file_type != "gif":
            raise ValueError("Only png,jgp and gif images are accepted "+str(file_type))
        """

        try:
            Image.open(file_path)
        except Exception:
            raise ValueError("Something goes wrong. I can't see the image. Image file may be corrupted")

        return True
