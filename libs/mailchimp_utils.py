import mailchimp

def get_mailchimp_api():
    return mailchimp.Mailchimp('8b7abae0903e1cc94be11f86aea80011-us9')


def mailchimp_batch_subs():
    m = get_mailchimp_api()

    array = []
    users = CUser.objects(email__exists=True)
    for user in users:
        array.append(
            {
                'email': {'email': user.email},
                'merge_vars': {'fname': user.f_name, 'lname': user.l_name},
            },
        )

    logging.debug(array)

    try:
        m.lists.batch_subscribe(
            "fb9e25041c",
            array,
            double_optin=False
        )
    except mailchimp.Error, e:
        messages.error(request,  'An error occurred: %s - %s' % (e.__class__, e))
        logging.debug('An error occurred: %s - %s' % (e.__class__, e))
