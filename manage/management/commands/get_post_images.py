import logging, cStringIO, time, gc
import boto, os
from boto.s3.key import Key
from django.core.management.base import BaseCommand, CommandError
from account.models import CUser
from collection.models import Collection, Item
from PIL import Image, ImageOps
import requests
from django.core.paginator import Paginator


def fetch_images(objects):
    for i in objects:
        if 'image' in i:
            try:
                if 'large' in i.image:
                    filename = i.image['large']['filename']
                    file_path = ("/var/prj/collexio/media/www/"+filename)
                    send_to_s3(file_path)
                    gc.collect()
                if 'small' in i.image:
                    filename = i.image['small']['filename']
                    file_path = ("/var/prj/collexio/media/www/"+filename)
                    send_to_s3(file_path)
                    gc.collect()
                if 'medium' in i.image:
                    filename = i.image['medium']['filename']
                    file_path = ("/var/prj/collexio/media/www/"+filename)
                    send_to_s3(file_path)
                    gc.collect()
                if 'original' in i.image:
                    filename = i.image['original']['filename']
                    file_path = ("/var/prj/collexio/media/www/"+filename)
                    send_to_s3(file_path)
                    gc.collect()
                if 'thumb' in i.image:
                    filename = i.image['thumb']['filename']
                    file_path = ("/var/prj/collexio/media/www/"+filename)
                    send_to_s3(file_path)
                    gc.collect()
            except Exception, e:
                print e

        print i.title


def send_to_s3(file_path):
        AWS_ACCESS_KEY_ID = "AKIAJ5OLQHKPRRNSKZYQ"
        AWS_SECRET_ACCESS_KEY = "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"

        conn = boto.connect_s3(AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY)
        bucket = conn.get_bucket('collexio-media')

        file_name = os.path.basename(file_path)

        k = Key(bucket)
        k.key = file_name
        k.set_contents_from_filename(file_path)

class Command(BaseCommand):
    def handle(self, *args, **options):
        Items = Item.objects.all()
        p = Paginator(Items, 50)
        for num in p.page_range:
            print "==============================="
            print "NEXT PAGE"
            print "==============================="
            it = p.page(num)
            fetch_images(it)

