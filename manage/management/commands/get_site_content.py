import logging, cStringIO, time, gc
from django.core.management.base import BaseCommand, CommandError
from account.models import CUser
from collection.models import Collection, Item, get_url_data
from PIL import Image, ImageOps
import requests


class Command(BaseCommand):
    def handle(self, *args, **options):
        url = "http://uber.com"
        item_data = get_url_data(url)
        print item_data