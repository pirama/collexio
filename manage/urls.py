from django.conf.urls import patterns, url

urlpatterns = patterns(
    '',
    url(r'^$', 'manage.views.collections', name='manage-collections'),
    url(r'^collections/$', 'manage.views.collections', name='manage-collections'),
    url(r'^users/$', 'manage.views.users', name='manage-users'),
    url(r'^update_featured/$', 'manage.views.update_featured_user_status', name='update-featured'),
    url(r'^twitter_usernames/$', 'manage.views.twitter_usernames', name='manage-twitter_usernames'),
)
