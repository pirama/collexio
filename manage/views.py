from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import user_passes_test
from collection.models import *
from account.models import CUser
from django.http import HttpResponse
import json


@user_passes_test(lambda u: u.is_superuser)
def collections(request):
    c_list = Collection.objects().order_by('-created')
    ctx = {'collections': c_list, 'tab': 'collections'}
    return render_to_response('manage_collection_list.html', ctx, context_instance=RequestContext(request))


@user_passes_test(lambda u: u.is_superuser)
def users(request):
    profiles = CUser.objects.filter().order_by('date_joined')
    ctx = {'profiles': profiles, 'tab': 'users'}
    return render_to_response('manage_user_list.html', ctx, context_instance=RequestContext(request))


@user_passes_test(lambda u: u.is_superuser)
def update_featured_user_status(request):
    result = {}
    user_pk = request.GET.get('user', False)
    if request.GET.get('user', False):
        user = CUser.objects.get(pk=user_pk)
        user.update_featured()
        print user
        result = {'success': 'User succesfully updated'}
    else:
        result = {'error': 'Empty user request'}
    return HttpResponse(json.dumps(result))


@user_passes_test(lambda u: u.is_superuser)
def twitter_usernames(request):
    profiles = CUser.objects.filter().order_by('date_joined')
    ctx = {'profiles': profiles}
    return render_to_response('manage_twitter_usernames.html', ctx, context_instance=RequestContext(request))
