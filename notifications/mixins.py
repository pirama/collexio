from notifications.models import Notifications

class NotificationMixin(object):

    @property
    def nt_count(self):
        return Notifications.objects(to=self, _is_seen=False).count()
