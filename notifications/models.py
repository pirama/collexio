from collection.models import *
from account.models import CUser

class Notifications(Document):
    
    meta = {'allow_inheritance': True}

    to = ReferenceField(CUser, reverse_delete_rule=CASCADE)
    nt_from = ReferenceField(CUser)
    collection = ReferenceField(Collection, reverse_delete_rule=CASCADE, required=False)
    item = ReferenceField(Item, reverse_delete_rule=CASCADE, required=False)
    created = DateTimeField()
    seen_on = DateTimeField()
    _is_seen = BooleanField(default=False)
    is_accepted = BooleanField(default=False)

    @property
    def message(self):
        return None

    @property
    def slack_payload(self):
        return None

    @property
    def is_seen(self):
        if self._is_seen:
            return True
        else:
            self._is_seen = True
            self.save()

    def send_email(self):
        return None

    def send_slack(self):
        url = "https://hooks.slack.com/services/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"
        requests.post(url, data=json.dumps(self.slack_payload))

    def send_nt(self):
        self.send_email()
        self.send_slack()
        self.save()

    def save(self, *args, **kwargs):
        if not self.created:
            self.created = datetime.now(tzutc())

        return super(Notifications, self).save(*args, **kwargs)


class FollowedNotification(Notifications):
    @property
    def message(self):
        return "followed you."

    def send_email(self):
        self.to.send_mail('A new follower for you on Collex.io!', 'follow.html', {'follower': self.nt_from})

    @property
    def slack_payload(self):
        return {
            'text': '<http://collex.io/' + self.nt_from.username + '|' + self.nt_from.name +
            '> Started to follow <http://collex.io/'+self.to.username+'|'+self.to.name+'>'
        }


class NewCollectionNotification(Notifications):
    @property
    def slack_payload(self, *args, **kwargs):
        return {
            'text': '<http://collex.io/' + self.nt_from.username + '|' + self.nt_from.name +
            '> Created a collection <http://collex.io/c/'+self.collection.slug+'|'+self.collection.title+'>',
        }

    def send_email(self):
        pass

    def save(self, *args, **kwargs):
        pass


class NewUserNotification(Notifications):
    @property
    def slack_payload(self, *args, **kwargs):
        return {
            'text': 'NEW USER! <http://collex.io/' + self.nt_from.username + '|' + self.nt_from.name +
            '> Joined Collexio',
            "color": "#ec7063"
        }

    def send_email(self):
        pass

    def save(self, *args, **kwargs):
        pass


class SignInNotification(Notifications):
    @property
    def slack_payload(self, *args, **kwargs):
        return {
            'text': '<http://collex.io/' + self.nt_from.username + '|' + self.nt_from.name +
            '> Signed In'
        }

    def send_email(self):
        pass

    def save(self, *args, **kwargs):
        pass


class RecollectNotification(Notifications):
    @property
    def message(self):
        return 'Recollect your <a href="'+self.item.collexio_url+'">item</a>'

    def send_email(self):
        self.to.send_mail('A new follower for you on Collex.io!', 'follow.html', {'follower': self.nt_from})


class SuggestionNotification(Notifications):
    @property
    def message(self):
        #return "hello"
        return 'Suggested to you an <a href="#" class="nt-item-link" data-item-id="'+str(self.item.pk) + '" data-nt-id="'+str(self.pk)+'">item</a>'

    @property
    def is_seen(self):
        return self._is_seen

    def send_email(self):
        self.to.send_mail('New Suggestion', 'suggested_item.html', {'item': self.item, 'collection': self.collection})
        return None


class ImportCompleteNotification(Notifications):
    import_source = StringField()

    @property
    def message(self):
        #return "hello"
        return 'Your collections imported from '+self.import_source+' and ready now.'

    def send_email(self):
        self.to.send_mail(
            'Your '+self.import_source+' collections imported to Collexio',
            'import_complete.html',
            {'import_source': self.import_source}
        )
        return None


class CollaborationNotification(Notifications):

    @property
    def slack_payload(self, *args, **kwargs):
        pass
        """
        return {
            'text': '<http://collex.io/' + self.nt_from.username + '|' + self.nt_from.name +
            '> added as a contributor on <http://collex.io/c/'+self.collection.slug+'|'+self.collection.title+'>',
        }
        """

    @property
    def message(self):
        #return "hello"
        return 'Invited you to contribute on <a href="http://collex.io/c/'+self.collection.slug+'">'+self.collection.title+'</a>'

    def send_email(self):
        self.to.send_mail(self.nt_from.name+' invited you to contribute', 'collaboration_notification.html', {'collection': self.collection,'from': self.nt_from})

