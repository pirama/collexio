from django.http import Http404


def check_content_type(allowed_types=False):
    if not allowed_types:
        allowed_types = ['item', 'collection', 'user', False]

    def method_wrapper(view_method):
        def arguments_wrapper(request, *args, **kwargs):
            if 'content_type' in kwargs:
                if kwargs.get('content_type') not in allowed_types:
                    raise Http404("Content type does not exists!")
            return view_method(request, *args, **kwargs)
        return arguments_wrapper
    return method_wrapper