from account.models import CUser
from collection.models import Collection, Item


def generate_search_data(search_query, content_type=False, only_my=False, username=False, page=1):
    # query structure for ElasticSearch
    filtered = {}
    filtered['query'] = {}
    filtered['query']['query_string'] = {}
    filtered['query']['query_string']['query'] = search_query
    filtered['filter'] = {}
    filtered['filter']['bool'] = {}
    filtered['filter']['bool']['must_not'] = [{"term": {"is_private": True}}, {"term": {"is_unlisted": True}}]
    filtered['filter']['bool']['must'] = []

    size = 10

    if content_type == 'collection':
        size = 12

    search_data = {}

    if search_query:
        if not (only_my or content_type):
            search_data = {
                "size": size,
                "from": (page-1) * 10,
                "query": {"filtered": filtered}
            }
        elif only_my:
            profile = CUser.objects.get(username=username)
            filtered['filter']['bool']['must'] = [{"term": {"owner": str(profile.pk)}}, {"term": {"_type": content_type}}]
            search_data = {
                "size": size,
                "from": (page-1) * 10,
                "query": {"filtered": filtered}
            }
        else:
            filtered['filter']['bool']['must'] = [{"term": {"_type": content_type}}]
            search_data = {
                "size": size,
                "from": (page-1) * 10,
                "query": {"filtered": filtered}
            }
    return search_data


def autocomplete_serializer(raw_data=False):
    data = []
    if raw_data:
        for item in raw_data:
            data.append({
                'title': item.title,
                'author': item.owner.username,
                'url':  item.collexio_url,
                'thumb': item.get_cover_thumb,
                'created': (item.created).strftime("%b %d")
            })
    return data


def results_serializer(raw_result, content_type, page):
    result = {}

    size = 10

    if content_type == 'collection':
        size = 12

    result['data'] = {}

    if 'hits' in raw_result:
        if len(raw_result['hits']['hits']) > 0:
            result['total'] = raw_result['hits']['total']
            result['page'] = page
            result['has_next'] = True if (int(raw_result['hits']['total']) > page*size) else False
            result['next_page_number'] = page + 1

            pks = []
            for item in raw_result['hits']['hits']:
                pks.append(item['_id'])

            if content_type == 'item':
                result['data'] = Item.objects.filter(pk__in=pks).order_by('-added')
            elif content_type == 'collection':
                result['data'] = Collection.objects.filter(pk__in=pks).order_by('-added')
            else:
                result['data'] = CUser.objects.filter(pk__in=pks)
    return result