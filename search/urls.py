from django.conf.urls import patterns, include, url

urlpatterns = patterns(
    '',
    url(r'^$', 'search.views.search', name='search'),
    url(r'^autocomplete/$', 'search.views.mdb_search_autocomplete', name='autocomplete'),
    url(r'^(?P<content_type>\w+)/$', 'search.views.search', name='search_by_content_type'),
)