# -*- coding: utf-8 -*-
import json

from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.template import RequestContext
from django.conf import settings
from django.contrib import messages

from elasticsearch import Elasticsearch
from elasticsearch.exceptions import ConnectionTimeout
from mongoengine import Q
from search.decorators import check_content_type
from collection.models import Collection
from search.serializers import generate_search_data, autocomplete_serializer, results_serializer


@check_content_type(allowed_types=['collection'])
def mdb_search_autocomplete(request, content_type='collection'):
    search_query = request.GET.get('q', False)  # search query
    result = {}

    if search_query:
        result = Collection.objects.filter(Q(title__istartswith=search_query) | Q(title__icontains=search_query))[:5]
    return HttpResponse(json.dumps(autocomplete_serializer(result)))

@check_content_type(allowed_types=['collection'])
def search_autocomplete(request, content_type='collection'):
    search_query = request.GET.get('q', False)  # search query
    result = {}

    if search_query:
        search_query = [search_query, "{0}*".format(search_query), "*{0}*".format(search_query)]
        for q in search_query:
            search_data = generate_search_data(q, content_type, only_my=False, username=False)
            es = Elasticsearch([settings.ES_URL])
            raw_result = es.search(
                index=['collexio'],
                fields=['_id'],
                doc_type=[content_type],
                body=json.dumps(search_data)
            )

            if 'hits' in raw_result:
                if len(raw_result['hits']['hits']) > 0:
                    pks = []
                    for item in raw_result['hits']['hits']:
                        pks.append(item['_id'])

                    result['data'] = Collection.objects.filter(pk__in=pks).order_by('-added')

                    if result['data']:
                        break

    return HttpResponse(json.dumps(autocomplete_serializer(result['data'])))


@check_content_type()
def search(request, content_type=False):

    search_query = request.GET.get('q', False)  # search query
    only_my = request.GET.get('only_my', False)  # search on your collections
    page = int(request.GET.get('page', 1))
    username = False
    result = {}

    if request.user.is_authenticated():
        username = request.user.username

    if search_query:
        # Do many searches until success. Strict search first.
        search_queries = [search_query, "{0}*".format(search_query), "*{0}*".format(search_query)]
        for q in search_queries:

            search_data = generate_search_data(q, content_type, only_my, username, page)
            es = Elasticsearch([settings.ES_URL])
            try:
                raw_result = es.search(
                    index=['collexio'],
                    fields=['_id'],
                    doc_type=[content_type],
                    body=json.dumps(search_data)
                )
            except ConnectionTimeout:
                messages.error(request, "We can't search this right now. Please check back later.")
                break

            result = results_serializer(raw_result, content_type, page)

            if result['data']:
                break

    context_dict = {
        "result": result,
        "type": content_type,
        "q": search_query,
        "only_my": only_my
    }

    if request.is_ajax():
        #  TODO: Create template for ajax searching
        return render_to_response('types/ajax_search.html', context_dict, context_instance=RequestContext(request))
    else:
        return render_to_response('search.html', context_dict, context_instance=RequestContext(request))