var isExtension = false;
var radius = 0;
/*
var interval = window.setInterval(function () {
    $(".logo a").css({
        "-webkit-mask": "-webkit-gradient(radial, 17 17, " + radius + ", 17 17, " + (radius + 15) + ", from(rgb(0, 0, 0)), color-stop(0.5, rgba(0, 0, 0, .2)), to(rgb(0, 0, 0)))"
    });
    radius++;
    if (radius === 200) {
        window.clearInterval(interval);
    }
}, "fast");
*/

$(".dropdown-menu").click(function (event) {
    event.stopPropagation();
});

$("[data-toggle='tooltip']").tooltip();
$(".add-tooltip").tooltip();

$("a").each(function () {
    var a = new RegExp("/" + window.location.host + "/");
    if (!a.test(this.href)) {
        $(this).click(function (event) {
            event.preventDefault();
            event.stopPropagation();
            window.open(this.href, "_blank");
        });
    }
});

$("#tags").tagsInput();

if ($("#c-cover").length) {
    var loader = new ImageLoader("#c-cover", {
        placeholder: ".collection-hero",
        method: "css"
    });
}

if ($(".btn-follow").length) {
    $(".btn-follow").followBtn();
}

jQuery.timeago.settings.strings = {
    prefixAgo: null,
    prefixFromNow: null,
    suffixAgo: "",
    suffixFromNow: "",
    seconds: "1m",
    minute: "1m",
    minutes: "%dm",
    hour: "1h",
    hours: "%dh",
    day: "1d",
    days: "%dd",
    month: "1mo",
    months: "%dmo",
    year: "1yr",
    years: "%dyr",
    wordSeparator: " ",
    numbers: []
};

$('div.timeago').timeago();


$(".item-video").on("click", function () {
    var w = $(this).width();
    var h = $(this).height();
    $(this).find("iframe").width(w);
    $(this).find("iframe").height(h);
    $(this).find("iframe").attr('src', function () {
        return $(this).data('src');
    });

    $(this).find(".iframe").show();
    $(this).find(".preview").hide();
});

function scroll_to_top() {
    var windowWidth = $(window).width(),
        didScroll = false;


    var $up = $("#go-to-top");

    $up.click(function (event) {
        $("html, body").animate({
            scrollTop: "0"
        });
        event.preventDefault();
    })

    $(window).scroll(function () {
        didScroll = true;
    });

    setInterval(function () {
        if (didScroll) {
            didScroll = false;

            if ($(window).scrollTop() > 200) {
                $up.css("display", "block");
            } else {
                $up.css("display", "none");
            }
        }
    }, 250);
}
scroll_to_top();




$(document).ready(function () {
    $('.embedly-url').embedly({
        key: '1457678e02504979b6c7eb1170968ddd'
    });

    var iframes = $('.video-item-iframe iframe');

    iframes.attr('data-src', function () {
        var src = $(this).attr('src');
        $(this).removeAttr('src');
        return src;
    });

    $('*').on('touchstart', function () {
        $(this).trigger('hover');
    }).on('touchend', function () {
        $(this).trigger('hover');
    });

    $(".btn-featured-collections").click(function () {
        console.log($('#featured').offset().top);
        $('body,html').animate({
            scrollTop: $('#featured').offset().top
        }, 'slow');
    });


    if ($('.messages > .message-text').html() != undefined) {
        showMessage();
    }
});

function showMessage(tag, message) {
    console.log(tag);
    console.log(message);

    if (tag != undefined && message != undefined) {
        $('.messages').html('<div class="message">' + message + '</div>');
        if (tag == "error") {
            $('.messages .message').removeClass('success');
            $('.messages .message').addClass('danger');
        } else {
            $('.messages .message').removeClass('danger');
            $('.messages .message').addClass('success');
        }
    }

    $('.messages').show();
    $('.messages').removeClass('bounceOutUp');
    $('.messages').addClass('animated bounceInDown');

    setTimeout(function () {
        $('.messages').removeClass('bounceInDown');
        $('.messages').addClass('bounceOutUp');
    }, 3500);
}

$('#create-collection-btn').on('click', function () {
    $('#signInModal h3').html("Sign in to start creating your collections!!");
    $('#signInModal').modal("show");
});

$('.sidebar-btn').on('click', function () {
    $('html').toggleClass('sidebar-open');
    $('body').toggleClass('sidebar-open');
    $('.sidebar').toggleClass('sidebar-open');
    $('.content').toggleClass('content-sidebar-open');
    $('.sidebar-btn').find('i').toggleClass('ion-close-round');
    $('.sidebar-btn').find('i').toggleClass('ion-navicon');
});


var $text = $('.side-bar .top-menu .dropdown').find('span'),
    $links = $('.side-bar .top-menu .dropdown-menu').find('a');

$links.on('click', function () {
    $text.text($(this).text());
    $('.side-bar .top-menu .dropdown').removeClass('open');
});




var nav = $('.top-nav');
var navOffset = nav.offset().top;

$(document).ready(function () {

    $(document).on('click', '#searchTrigger', function () {
        $('.icon-search-container').addClass('active');
        $('.btn-close').addClass('active');
        $('.content  .top-menu').addClass('hidden');
        $('.logo').addClass('hidden');
        $('#searchInput').focus();
    });

    $(document).on('blur', '#searchInput', function () {
        if ($('#searchInput').val().length > 0) {
            $(this).text('');
        } else {
            $('#searchTrigger').removeClass('active');
        }
    });

    $(document).on('click', '#searchClear', function () {
        $('.icon-search-container').removeClass('active');
        $('.content .top-menu').removeClass('hidden');
        $('.logo').removeClass('hidden');
        $('.auto-complete-results').removeClass('active');
        $('#searchInput').val('');
        $('.search-result-header').removeClass('active');
    });

    $(document).on('focus', '#searchInput', function () {
        $('#searchTrigger').addClass('active');
    });

    $(document).on('keyup focus', '#searchInput', function (event) {
        var _tmpsearchInput_val=$('#searchInput').val();

        if($('.search-result-header').hasClass('active'))return;

        $('#searchTrigger').addClass('active');
        if (_tmpsearchInput_val.length > 0) {
            $('.auto-complete-results').addClass('active');
            $('.auto-complete-results>.preloader').show();
            $('.auto-complete-results>.container').empty();
            $.ajax({
                dataType: "json",
                url: '/search/autocomplete/?q='+encodeURIComponent(_tmpsearchInput_val),
                success: function(data,textStatus,jqXHR){
                    console.log('searchInput',data);
                    // Old result filter
                    if($('#searchInput').val()!=_tmpsearchInput_val || $('.auto-complete-results>.container li').length)return;
                    $('.auto-complete-results>.preloader').hide();
                    for(var i in data){
                        $('<li class="r-item">'+
                            (String(data[i].thumb).toLowerCase()=='null'?'<div class="gray_img"></div>':'<img src="'+data[i].thumb+'">')+
                            '<div class="title">'+
                                '<a href="'+data[i].url+'" class="track-item-link-click">'+data[i].title+'</a>'+
                            '</div>'+
                            '<div class="user-info"> '+
                                'by '+
                                '<a class="collection-title" href="/'+data[i].author+'/">'+data[i].author+'</a> '+
                                '<span class="date">'+data[i].created+'</span>'+
                            '</div>'+
                            '<div class="clearfix"></div>'+
                        '</li>').appendTo($('.auto-complete-results>.container'));
                    }
                    if(data.length==0){
                        $('<li class="r-item">'+
                            '<center><b>No result! :(</b></center>'+
                        '</li>').appendTo($('.auto-complete-results>.container'));
                    }
                }
            });

        } else {
            $('.auto-complete-results').removeClass('active');
        }
        if (event.which === 13) {
            $('.auto-complete-results').removeClass('active');
            $('.search-result-header').addClass('active');
            $('.more-result').click();
        }
    });

    $(document).on('click', '.more-result', function (e) {

        var _tmpsearchInput_val=$('#searchInput').val();
        /*
        $('.auto-complete-results').removeClass('active');
        $('.search-result-header').addClass('active');
        e.preventDefault();
        */
        document.location.href='/search/collection/?q='+encodeURIComponent(_tmpsearchInput_val);
        e.preventDefault();
    });


    if ($(document).width() < 480) {
        $("#searchInput").attr("placeholder", "Write a text to search");
    }

    if ($('section').is('#sidebar_container_static') || $('section').is('#sidebar_container_static_featured')) {
        $('.feed .container').css('margin', '0 0 0 30%');
    }



    $('.user-select input').on('click', function (el) {

        $.ajax({
            type: "GET",
            url: "/r/a/follow/" + $(this).data("id") + "/",
            dataType: "json",
            success: function (data) {
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#messageModal').modal("show");
            },
            complete: function () {}
        });

    });
    
    $("#visibility").change(function () {
        var str = "";
        str = $("#visibility option:selected").data('description');
        $("#description").text(str);
    })


});