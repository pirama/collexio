$(window).scroll(function() {
	var y = $(window).scrollTop();
	if (y > 608) {
		
		//$(".collection-nav-fixed").addClass('navbar-fixed-top');
		$(".collection-nav-fixed").addClass('active');
	} else {
		//$(".collection-nav-fixed").removeClass('navbar-fixed-top');
		$(".collection-nav-fixed").removeClass('active');
	}
});

$(window).scroll(function() {
	var y = $(window).scrollTop();
	var loadStartingHeight = $(window).height()-375
	if (y > loadStartingHeight) {
	}
});

$('.c-list .btn-load-more').on('click',function(){
	var listType = $('#pagingData').data("list");
	var page = $('#pagingData').data("next");
	var profile = $('#pagingData').data("profile");
	$.ajax({
		type: "GET",
		url: "/c/list/"+listType+"/",
		data: {
			'p':page,
			'a':profile
		},
		dataType: "html",
		success: function(data) {
			$('#pagingData').remove();
			$('#collectionList').append(data);
			console.log($('#pagingData').length);

			if ($('#pagingData').length==0){
				$('#loadMoreWrapper').remove();
			}
		},
		error: function(jqXHR,textStatus,errorThrown) {
		},
		complete: function(){
		}
	});
});

$('#btn-add-item').on('click',function(){
	$('#addItemModal').modal("show");
});

$('#btn-c-settings').dropdown();
$('#btn-c-settings').tooltip();

//$('#bottom-popup').modal('show');

var popupClosed = 0;

$(window).scroll(function() {
	var y = $(window).scrollTop();
	if (y > 675) {
		if (popupClosed!=1) $("#bottom-popup").slideDown();
	} else {
		$("#bottom-popup").slideUp();
	}
});

$(".popup-header > button.close").on("click",function(){
	$("#bottom-popup").slideUp();
	popupClosed = 1;
});

$('#recommendations-bar').on('click',function(){
	var recommendation_count = $('#recommendations-bar').attr('data-count');

	if ($('.recommended-items').css('display')=="block"){
		$('#recommendations-bar').html("You have "+recommendation_count+" recommendations for this collection");
		$('.recommended-items').hide();
	} else {
		$('#recommendations-bar').html("Hide recommedations");
		$('.recommended-items').show();
	}
	
});

var pinboardContainer = $('.pinboard .items-wrapper');
applyLayout();

function applyLayout(){
	if (pinboardContainer.masonry){
		pinboardContainer.masonry('destroy');	
	}
	
	$('.c-item').each(function(item){
		var thumb = $(this).find('.magazine-thumb');
		var w = thumb.attr('data-width');
		var h = thumb.attr('data-height');
		thumb.height((thumb.width()*h)/w);

	});
	pinboardContainer.masonry({
	  columnWidth: 	'.c-item',
	  gutter: 32,
	  "isFitWidth": true,
	  itemSelector: '.c-item'
	});
}

itemLoadMoreBtn = $('.c-items .btn-load-more');
itemLoadMoreBtn.on('click',function(){
	var page = $('#pagingData').attr('data-next');
	var collection = $('#pagingData').attr('data-collection');

	$.ajax({
		type: "GET",
		url: "/r/c/"+collection+"/items/",
		data: {
			'p':page,	
		},
		dataType: "json",
		success: function(data) {
			$('.items-wrapper').append(JSON.parse(data['html']));	
			if (pinboardContainer){
				pinboardContainer.masonry('appended',JSON.parse(data['html']));
				$('.c-item').each(function(item){
					var thumb = $(this).find('.magazine-thumb');
					var w = thumb.attr('data-width');
					var h = thumb.attr('data-height');
					thumb.height((thumb.width()*h)/w);

				});
				pinboardContainer.masonry('reloadItems');
				pinboardContainer.masonry();
			}
			

			$('.edit-item').off();
			$('.edit-item').on('click',function(event){
				editItemform(event,this);
			});

			$(".delete-item").off();
			$(".delete-item").on("click",function(event){
				deleteItem(event,this);
			});
			
			$('.embedly-url').embedly({key: '1457678e02504979b6c7eb1170968ddd'});

			$(".item-video").on("click", function() {
				var w = $(this).width();
				var h = $(this).height();
				$(this).find("iframe").width(w);
				$(this).find("iframe").height(h);
				$(this).find("iframe").attr('src', function() {
			        return $(this).data('src');
			    });

				$(this).find(".iframe").show();
				$(this).find(".preview").hide();
			});
			
			$('#pagingData').attr('data-next', data['p']['next_page_number']);
			$('#pagingData').attr('data-has-next', data['p']['has_next']);

			if ($('#pagingData').attr('data-has-next')=="false"){
				$('#loadMoreWrapper').remove();
			}
			/*
			$('#pagingData').remove();
			$('#collectionList').append(data);
			console.log($('#pagingData').length);			
			*/

			//applyLayout();
			
			
			
		},
		error: function(jqXHR,textStatus,errorThrown) {
			//$('#messageModal').modal("show");
			console.log(textStatus);
		},
		complete: function(){
		}
	});
});


function changeSubscriptionState(){
	count = parseInt($('.subscriptions-count .count').html(), 10);

	if ($('.subscribe-btn').attr('data-is-subscribed')=="true"){
		$('.subscribe-btn').attr('data-is-subscribed','false');
		$('.subscribe-btn').html('SUBSCRIBE');
		$('.subscribe-btn').removeClass('subscribed');
		$('.subscribe-btn').addClass('outline');
		$('.subscribe-btn.subscribed').off('mouseover mouseout');

		$('.subscriptions-count .count').html(count-1)
		if (count==1){
			$('.subscriptions-count').hide();
		}
		
	} else {
		$('.subscribe-btn').attr('data-is-subscribed','true');
		$('.subscribe-btn').html('SUBSCRIBED');
		$('.subscribe-btn').removeClass('outline');
		$('.subscribe-btn').addClass('subscribed');

		

		$('.subscriptions-count .count').html(count+1)
		if (count>=0){
			$('.subscriptions-count').show();
		}
		attach_subscribed_events();

	}
}

function attach_subscribed_events(){

	$('.subscribe-btn.subscribed').on('mouseover',function(){
		if ($('.subscribe-btn').attr('data-is-subscribed')=="true"){
			$(this).html("Unsubscribe");
		}
	});

	$('.subscribe-btn.subscribed').on('mouseout',function(){
		if ($('.subscribe-btn').attr('data-is-subscribed')=="true"){
			$(this).html("Subscribed");
		}
	});	
}

attach_subscribed_events();

if (parseInt($('.subscriptions-count .count').html(), 10)>=1){
	$('.subscriptions-count').show();
}


$('.subscribe-btn').on('click',function(){
	var collectionId = $(this).attr('data-c-id');
	var is_subscribed = $(this).attr('data-is-subscribed');

	if (is_subscribed=="false"){
		var url = "/r/c/"+collectionId+"/subscribe/"
		var success_message = "You've subscribed to collection!"
	} else {
		var url = "/r/c/"+collectionId+"/unsubscribe/" 
		var success_message = "You've unsubscribed to collection!"
	}

	$.ajax({
		type: "GET",
		url: url,
		dataType: "json",
		success: function(data) {
			if (data.messages.error){
				showMessage("error",data.messages.error);
			} else {
				showMessage("success",success_message);
	        	changeSubscriptionState();	
			}
	        
		},
		error: function(jqXHR,textStatus,errorThrown) {
			showMessage("error","Ups! Something goes wrong");
		},
		complete: function(){
		}
	});
});


function changeRecommendationState(){

	if ($('#recommend-btn').attr('data-is-recommended')=="true"){
		$('#recommend-btn').attr('data-is-recommended','false');
		$('#recommend-btn .button-text').html('RECOMMEND');
		$('#recommend-btn').addClass('outline');
	} else {
		$('#recommend-btn').attr('data-is-recommended','true');
		$('#recommend-btn .button-text').html('RECOMMENDED');
		$('#recommend-btn').removeClass('outline');
	}
}

$('#recommend-btn').on('click',function(){
	var collectionId = $(this).attr('data-c-id');
	var is_recommended = $(this).attr('data-is-recommended');

	if (is_recommended=="false"){
		var url = "/r/c/"+collectionId+"/recommend/"
		var success_message = "You've recommended this collection to your followers!"
	} else {
		var url = "/r/c/"+collectionId+"/undorecommend/" 
		var success_message = "You've undo your recommendation!"
	}

	$.ajax({
		type: "GET",
		url: url,
		dataType: "json",
		success: function(data) {
			if (data.messages.error){
				showMessage("error",data.messages.error);
			} else {
				showMessage("success",success_message);
	        	changeRecommendationState();	
			}
	        
		},
		error: function(jqXHR,textStatus,errorThrown) {
			showMessage("error","Ups! Something goes wrong");
		},
		complete: function(){
		}
	});
});




/*
Pinboard plugin
https://github.com/GBKS/Wookmark-jQuery/blob/master/example-endless-scroll/index.html


var container = $('.pinboard > #itemsContainer');
var handler = $('.c-item');

var options = {
	container: container,
	itemWidth: 368,
	flexibleWidth: 0,
	autoResize:true,
	offset: 16,
	outerOffset: 16
}

$(document).ready(function(){
	applyLayout();

});

function applyLayout() {

	// Destroy the old handler
	if (handler.wookmarkInstance) {
		handler.wookmarkInstance.clear();
	}
	// Create a new layout handler.
	handler = $('.c-item');
	handler.wookmark(options);

    container.imagesLoaded(function() {
    	applyLayout();
    });
}
*/

/*
$(window).scroll(function() {
	var y = $(window).scrollTop();
	if (y > 675) {
		ga('send', 'event',{
		  'eventCategory': 'page-view',
		  'eventAction': 'scroll',
		  'eventLabel': 'collection-page-scroll',
		});
	}
});
*/