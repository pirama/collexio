feedLoadMoreBtn = $('.feed .load-more');

feedLoadMoreBtn.on('click',function(){
	var user = $(this).attr('data-user-id');
	var since_id = $('.c-items').find('.items-wrapper .c-item').last().attr('data-activity-id');
	
	$.ajax({
		type: "GET",
		url: "/r/f/activities/",
		data: {
			'since_id':since_id,	
		},
		dataType: "json",
		success: function(data) {
			var html = String(data['html']);
			if (html.length<=2){
				console.log("nothing to show");
				$('#loadMoreWrapper > .btn').hide();
				$('#loadMoreWrapper > .eof-message').show();
			} else {
				$('.items-wrapper').last().append(JSON.parse(data['html']));
				console.log("data added");
			}
		},
		error: function(jqXHR,textStatus,errorThrown) {
			//$('#messageModal').modal("show");
			console.log(textStatus);
		},
		complete: function(){
		}
	});
});


$(window).scroll(function () {
    var winScroll = $(window).scrollTop();
    if (winScroll > 70) {
        $('.right-panel').addClass('fixed');
    } else {
        $('.right-panel').removeClass('fixed');
    }
});
