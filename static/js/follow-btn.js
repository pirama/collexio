(function ($) {

    $.fn.followBtn = function() {
    	this.each(function() {
    		if ($(this).data('state')=="True"){
    			changeState($(this),"following");
    		} else if ($(this).data('state')=="None"){
    			$(this).on('click',function(){
    				$('#signInModal h3').html("Sign in to start following!");
    				$('#signInModal').modal("show");
    			});
    		} else {
    			changeState($(this),"notFollowing");
    		}
    	});
    };

   changeState = function(el, state){
   		if (state == "following"){

   			el.removeClass("btn-loading").addClass("btn-following");
			el.html("Following");
			el.prop("disabled","");

			el.off();

			el.on('mouseover',function(){
				el.html("Unfollow");
			});

			el.on('mouseout',function(){
				el.html("Following");
			});

			el.on('click',function(){
    			unfollowRequest(el);
    		});

    		

   		} else {
   			el.removeClass("btn-following");
			el.removeClass("btn-loading");
			el.html("Follow");
			el.prop("disabled","");

			el.off();

			el.on('click',function(){
    			followRequest(el);
    		});
   		}
   }

    followRequest = function(el){
    	var preloader = $("#preloader").clone();
    	el.addClass("btn-loading");
    	el.html(preloader);
    	preloader.show();
    	el.prop("disabled","disabled");

    	$.ajax({
			type: "GET",
			url: "/r/a/follow/"+el.data("id")+"/",
			dataType: "json",
			success: function(data) {
				changeState(el, "following");
				var followerCount = parseInt($(".number-box .follower-count").html());
    			$(".number-box .follower-count").html(followerCount+1);
			},
			error: function(jqXHR,textStatus,errorThrown) {
				$('#messageModal').modal("show");
				changeState(el, "notfollowing");
			},
			complete: function(){
			}
		});
    }

    unfollowRequest = function(el){
    	var preloader = $("#preloader").clone();
    	el.addClass("btn-loading");
    	el.html(preloader);
    	preloader.show();
    	el.prop("disabled","disabled");

    	$.ajax({
			type: "GET",
			url: "/r/a/unfollow/"+el.data("id")+"/",
			dataType: "json",
			success: function(data) {
				changeState(el, "notFollowing");
				var followerCount = parseInt($(".number-box .follower-count").html());
    			$(".number-box .follower-count").html(followerCount-1);
			},
			error: function(jqXHR,textStatus,errorThrown) {
				$('#messageModal').modal("show");
				changeState(el, "following");
			},
			complete: function(){
			}
		});
    }
    
    

 
}(jQuery));