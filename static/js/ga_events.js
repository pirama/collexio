$('[data-event-label="masthead-twitter-signin"]').click(function(){
	ga('send', 'event',{
	  'eventCategory': 'signin',
	  'eventAction': 'click',
	  'eventLabel': 'masthead-twitter-signin',
	});	
});

$('[data-event-label="modal-twitter-signin"]').click(function(){
	ga('send', 'event',{
	  'eventCategory': 'signin',
	  'eventAction': 'click',
	  'eventLabel': 'modal-twitter-signin',
	});
});

$('[data-event-label="bottom-twitter-signin"]').click(function(){
	ga('send', 'event',{
	  'eventCategory': 'signin',
	  'eventAction': 'click',
	  'eventLabel': 'bottom-twitter-signin',
	});
});

$('[data-event-label="topnav-create-signin"]').click(function(){
	ga('send', 'event',{
	  'eventCategory': 'signin',
	  'eventAction': 'click',
	  'eventLabel': 'topnav-create-signin',
	});
});

$('[data-event-label="collection-twitter-signin"]').click(function(){
	ga('send', 'event',{
	  'eventCategory': 'signin',
	  'eventAction': 'click',
	  'eventLabel': 'collection-twitter-signin',
	});
});

$('[data-event-label="endofcoll-create-signin"]').click(function(){
	ga('send', 'event',{
	  'eventCategory': 'signin',
	  'eventAction': 'click',
	  'eventLabel': 'endofcoll-create-signin',
	});
});

$('[data-event-label="profile-follow"]').click(function(){
	ga('send', 'event',{
	  'eventCategory': 'follow',
	  'eventAction': 'click',
	  'eventLabel': 'profile-follow',
	});
});

$('[data-event-label="followers-follow"]').click(function(){
	ga('send', 'event',{
	  'eventCategory': 'follow',
	  'eventAction': 'click',
	  'eventLabel': 'followers-follow',
	});
});

$('[data-event-label="collection-profile-follow"]').click(function(){
	ga('send', 'event',{
	  'eventCategory': 'follow',
	  'eventAction': 'click',
	  'eventLabel': 'collection-profile-follow',
	});
});

$('[data-event-label="actionbar-learn-more"]').click(function(){
	ga('send', 'event',{
	  'eventCategory': 'LearnMore',
	  'eventAction': 'click',
	  'eventLabel': 'actionbar-learn-more',
	});
});


