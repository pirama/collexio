$(function () {
	var progress = 0;
	if ($("#item-image").length) {
	    $('#item-image').fileupload({
	        dataType: 'json',
	        add: function(e,data){
	        	$(".messages").hide();
	        	//var file = e.originalEvent.delegatedEvent.target.files[0];
	        	var file = data.files[0];
	        	var file_type = file.type;
	        	var file_size = file.size;
	        	if (file_type != "image/jpeg" && file_type != "image/png" && file_type != "image/gif"){
	        		$(".messages").show();
	        		$(".modal-messages > span").html("Invalid file type. Jpeg, PNG and Gif files accepted.");
	        		return false;
	        	}
	        	if (file_size >= 5242880){
	        		$(".modal-messages").show();
	        		$(".modal-messages > span").html("Too big file. File size must be smaller than 5MB.");
	        		return false;
	        	}
	        	if (data.autoUpload || (data.autoUpload !== false &&
			            $(this).fileupload('option', 'autoUpload'))) {
			        data.process().done(function () {
			            data.submit();
			        });
			    }

	        },
	        done: function (e, data) {
	        	var image_el = '<img src="/'+data.result.file_path+'" class="add-item-image" />';
	            $('.item-image-preview').html(image_el);
	            $('#add-item-image-url').val(data.result.file_path);
	            $('#media-removed').val('False');
	        },
	        change: function (e, data) {
		    	progress = 0;
		    	$('#item-image-progress .bar').css(
		            'width',
		            progress + '%'
		        );  
		    },
	        progressall: function (e, data) {
		        progress = parseInt(data.loaded / data.total * 100, 10);
		        $('#item-image-progress .bar').css(
		            'width',
		            progress + '%'
		        );
		    }
	    });
	}
});

var deleteItem = function(event, el){
	$('#deleteItemModal').modal('show');
	var itemId = $(el).attr("data-item-id");
	var href = "/c/i/"+itemId+"/delete/";
	$("#deleteItemBtn").attr("href", href);
	event.preventDefault();
}

$(".delete-item").on("click",function(){
	deleteItem(event,this);
});

$('.add-item-selection .nav-pills li a').on('click',function(){
	$('.add-item-selection .nav-pills li').removeClass('active');
	

	var currentClass = $(this).parent().attr('class');
	$(this).parent().addClass('active');

	$('#addItemModal form').removeClass("link");
	$('#addItemModal form').removeClass("image");
	$('#addItemModal form').removeClass("note");

	if (currentClass == "link-item-btn"){
		$('#add-item-form').addClass("link");
	} else if (currentClass == "image-item-btn"){
		$('#add-item-form').addClass("image");
	} else {
		$('#add-item-form').addClass("note");
	}

});

function fetchURL(self){
	if (self.val()){
		$("#item-form").hide();
		$(".modal-footer").hide();
		
		$("#item-form-loading > span").html("I'm trying to get data from url");
		$("#item-form-loading").show();
		
		$.ajax({
			type: "GET",
			url: "/c/get_site_content/",  // or just url: "/my-url/path/"
			dataType: "json",
			contentType: "json",
			data: {
				url: self.val()
			},
			success: function(data) {
				if (data.preview.html){
					addCard(data);
				}else if (data.image!=undefined){
					addImage(data);
				}

				$('#add-item-source').val(data.source);
				$('#add-item-title').val(data.title);
				$('#add-item-description').val(data.description);
				$('#add-item-image-url').val(data.image);

				$('#item-form').show();
				$(".modal-footer").show();
				$('#item-form-loading').hide();
				$('.fetch-url').hide();
			},
			error: function(xhr, textStatus, errorThrown) {
				$('#item-form').show();
				$(".modal-footer").show();
				$('#item-form-loading').hide();
			}
		});	
	}
}


$("#add-item-form-url").keypress(function(e){
	var key = e.which;
	if(key == 13){
	    fetchURL($(this));
	}
});

var editItemform = function(event,el){
	event.preventDefault();
	var self = $(el);
	var itemId = self.attr('data-item-id');

	$('#addItemModal').modal('show');

	$("#add-item-form").get(0).reset();
	$(".item-image-preview img").attr("src", "");

	$("#item-form").hide();
	$(".modal-footer").hide();
	$("#item-form-loading > span").html("I'm loading item details");
	$("#item-form-loading").show();
	$("#add-item-form").attr('action','/r/i/'+itemId+'/edit/');

	$('#item-form-title').html('Edit Item');


	
	
	$.ajax({
		type: "GET",
		url: "/r/i/"+itemId+'/',  // or just url: "/my-url/path/"
		dataType: "json",
		contentType: "json",
		success: function(data) {
			
			data = data.item;
			if (data.preview){
				addCard(data);
			}else if (data.image!=undefined){
				editImage(data);
			}

			
			$('#add-item-id').val(itemId);
			$('#add-item-source').val(data.source);
			$('#add-item-title').val(data.title);
			$('#add-item-description').val(data.description);
			$('#add-item-form-url').val(data.url);

			
			$('#item-form').show();
			$(".modal-footer").show();
			$('#item-form-loading').hide();

		},
		error: function(xhr, textStatus, errorThrown) {
			$('#item-form').show();
			$(".modal-footer").show();
			$('#item-form-loading').hide();
		}
	});	
	
}

$('.edit-item').on('click',function(event){
	editItemform(event,this);
});

$("#add-item-form-save-btn").on('click',function(e){
	$("#item-form").hide();
	$("#url-form").hide();
	$("#item-form-loading > span").html("Saving.");
	$("#item-form-loading").show();
	e.preventDefault();
	var url = $("#add-item-form").attr('action');
	$.ajax({
		type: "POST",
		url: url,
		dataType: "json",
		data: $("#add-item-form").serialize(),
		beforeSend: function(xhr, settings) {
	        if (!this.crossDomain) {
	            xhr.setRequestHeader("X-CSRFToken", $("[name='csrfmiddlewaretoken']").val());
	        }
	    },
		success: function(data) {
			$(".spinner-icon").hide();

	        if (data.messages.error){
	        	$(".modal-messages").show();
	        	$(".modal-messages > span").html(data.messages.error);
	        	$("#item-form").show();
				$("#url-form").show();
				$('#item-form-loading').hide();
	        }
			else if (isExtension==true){
				$("#item-form-loading > span").html("You item has been saved!");
				setTimeout(function(){
					parent.postMessage('cx_close',"*");
				},2000);
			} else {
				$("#item-form-loading > span").html(data.messages.success);
				$("#item-form-loading > span").append("<br/> Please wait while redirect.")
				window.location.reload();
			}
		},
		error: function(jqXHR,textStatus,errorThrown) {
			$("#item-form").show();
			$("#url-form").show();
			$('#item-form-loading').hide();
		},
		complete: function(){
			
		}
	});
	return false;
});

$('.item-form-close').on('click',function(){
	resetItemForm();
});

$('#btn-suggest-item').on('click',function(){
	if ($(this).data('state')=="True"){

		$('#addItemModal').modal('show');
		$('#item-form-title').html('Suggest an Item');
		$('#suggested').val("True");

	} else {
		$('#signInModal h3').html("Sign in to suggest an item!");
		$('#signInModal').modal("show");
	}
});

var addImage = function(data){
	$('.item-image-preview').html('<img src="'+data.image+'" class="add-item-image"/>');
	$('#remove-img-btn').show();
	$('.card-preview').hide();
}

var editImage = function(data){
	$('.item-image-preview').html('<img src="'+data.image+'" class="add-item-image"/>');
	$('#remove-img-btn').show();
	$('.card-preview').hide();
}

var addCard = function(data){
	$('.card-preview').html(data.preview.html);
	$('.card-preview').fitVids({ customSelector: "iframe[src^='//vine.co'], iframe[src^='http://myviiids.com']"});
	$('.embedly-url').embedly({key: '1457678e02504979b6c7eb1170968ddd'});
	$('#card-data').val(JSON.stringify(data.card));
	$('.card-preview').show();
	$('.image-frame').hide();
}

var resetItemForm = function(){
	$("#item-form").hide();
	$('#addItemModal form').removeClass("link");
	$('#addItemModal form').removeClass("image");
	$('#addItemModal form').removeClass("note");
	$('#add-item-form').addClass("link");

	$("#url-form").show();
	$("#add-item-form").get(0).reset();
	$('#add-item-source').val(null);
	$('#add-item-image-url').val(null);
	$('.item-image-preview').html("");
	$('.image-frame').show();
	$('#card-data').val(null);
	$('#add-item-title').val(null);
	$('#item-form-loading').hide();
	$(".modal-messages").hide();
	$('.card-preview').hide();
}

$("#addItemModal").on('hidden.bs.modal',function(){
	resetItemForm();
});

$('#remove-img-btn').on('click',function(){
	$('#add-item-image-url').val(null);
	$('.item-image-preview').html("");
	$('.image-frame').show();
	$('#media-removed').val('True');
});
