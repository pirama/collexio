mixpanel.track_links(".track-signin","signin_btn_click",function(el){
	return {
		"label":$(el).attr('data-event-label'),
	}
});

mixpanel.track_links(".track-learnmore","learnmore_btn_click",function(el){
	return {
		"label":$(el).attr('data-event-label')
	}
});

mixpanel.track_links(".track-pre-signin","presignin_btn_click",function(el){
	return {
		"label":$(el).attr('data-event-label')
	}
});

mixpanel.track_links(".track-item-link-click","item-link-click",function(el){
	return {
		"label":$(el).attr('data-event-label'),
		"link":$(el).attr('href')
	}
});

$(".item-video").on("click", function() {
	mixpanel.track("video-play-click",{
		"link": $(this).find("iframe").attr('src')
	});
});

mixpanel.track_forms(".track-new-collection","new_collection");
mixpanel.track_forms(".track-edit-collection","edit_collection",function(el){
	return {
		"title": $(el).find('#collection-name').val()
	}
});



/*
$('[data-event]').on('click',function(){
	
	mixpanel.track_links(eventName,{
		"label": eventLabel
	});
});
*/