"use strict";
var app = angular.module('collaborators',[
	'ngResource',
	'ui.bootstrap', 
	'ui.bootstrap.typeahead'
]);
app.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';    }
]);

app.factory('Users', function($resource) {
	return $resource('/r/userlist_autocomplete');
});

app.factory('Collaborator', function($resource) {
	return $resource(
		'/r/c/:collectionId/collaborators/:userId/:action',
		{collectionId:'@collection_id'},
		{
			'query': {
				method: 'GET', 
				isArray: false 
			},
			'delete': { 
				method:'GET',
				params: {userId: '@userId',action: 'delete'}
			}
		}
	);
});

app.controller("addCollaboratorCtrl",function($scope,$http,Collaborator){

});

app.directive("collaboratorlist",function($http, $sce, Collaborator){
    return {
    	scope: {
    		cid: "@",
    	},
        restrict: "E",
        templateUrl: '/static/js/ng-collexio/collaborators/collaborators.html',
        link:function(scope, parent, element, attrs, http){
        	scope.permissionNames = {
        		'canview':'Can View',
        		'canadd': 'Can Add',
        		'admin': 'Admin'
        	};
        	scope.hello="";
        	scope.permission ="canview";
        	scope.collaborators = [];

        	var qres = Collaborator.query({collectionId:scope.cid},function(data) {
        		angular.forEach(data.result,function(value, key){
        			var newCollaborator = new Collaborator(value);
        			scope.collaborators.push(angular.copy(newCollaborator));
        		});
	        	
        	});
			

        	scope.to_label = function(perm){
			    return $sce.trustAsHtml(scope.permissionNames[perm]);
			}

			scope.userAutocomplete = function(val){
				return $http.get('/r/a/userlist_autocomplete/', {
			      params: {
			        username: val,
			      }
			    }).then(function(response){
			    	if (response){
			    		return response.data.results.map(function(item){
				        	return item;
				      	});
			    	}
			    });
			}

			scope.onSelect = function(item, model, label){
				scope.selectedUser = item;
				console.log(item);
			}

			scope.onAddBtnClick = function(){
				var user = angular.copy(scope.selectedUser); 
				var collaborator = {
					collection_id: scope.cid,
					user_id: user.id,
					user: user,
					permission: angular.copy(scope.permission)					
				}
				angular.forEach(scope.collaborators,function(c,key){
					if (c.user.id == collaborator.user.id){
						var idx = scope.collaborators.indexOf(c);
						scope.collaborators.splice(idx, 1);
					}
				});
				var newCollaborator = new Collaborator(collaborator)
				var c = angular.copy(newCollaborator);

				newCollaborator.$save().then(function(data){
					scope.collaborators.push(c);
				});
			}

			scope.onDeleteBtnClick = function(idx){
				console.log(idx);
				var collaborator = scope.collaborators[idx];
				collaborator.$delete({'collectionId':scope.cid, 'userId':collaborator.user.id});
				scope.collaborators.splice(idx, 1);
			}
        }
    }
});