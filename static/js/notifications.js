var mediaPath = "//collex.io/media/www/";
var nt_loadMore_btn = $('#notificationsModal .loadMoreWrapper');
var next_page_number = 1;


$('.btn-notifications').on('click',function(){
	
	$('.notifications-list').html('');
	$('#notificationsModal').modal("show");
	$('#notificationsModal .preloader').show();
	next_page_number = 1;
	get_notifications();
});

$('#ntItemModal .close').on('click',function(){
	$('#ntItemModal').modal('hide');
	$('#notificationsModal').modal("show");
});

nt_loadMore_btn.on('click',function(){
	get_notifications();
});


var get_notifications = function(){
	$.ajax({
		type: "GET",
		url: "/r/a/notifications/",  // or just url: "/my-url/path/"
		data: {
			'p':next_page_number
		},
		dataType: "json",
		contentType: "json",
		success: function(data) {
			console.log(data);

			if(data['next_page_number']!=undefined && data['next_page_number']>1){
				nt_loadMore_btn.show();
				next_page_number = data['next_page_number'];
			} else{
				nt_loadMore_btn.hide();
			}

			$.each(data['notifications'],function(index,nt){
				el = $('.nt-item.js-template').clone();

				var user_image_div = el.find('.nt-user-image');
				var user_image = el.find('.nt-user-image img');
				var user_image_link = el.find('.nt-user-image a');
				var user_name_div = el.find('.nt-user-name'); 
				var user_name_link = el.find('.nt-user-name a');
				var time = el.find('.nt-time');
				var nt_message = el.find('.nt-message');
				var nt_indicator = el.find('.nt-indicator');
				

				el.attr('id',nt['id']);

				if (nt['collection']!=undefined){
					el.attr('data-collection-name',nt['collection']['title']);	
				}

				if(nt['is_seen']==true){
					nt_indicator.hide();
				}
				
				user_image.attr('src',mediaPath+nt['from']['image']['thumb']['filename']);
				user_image_link.attr('href','/'+nt['from']['username']);
				user_name_link.html(nt['from']['name']);
				user_name_link.attr('href','/'+nt['from']['username']);

				console.log(nt['is_accepted']);

				if (nt['is_accepted']==true){
					nt['message'] = nt['message']+' <span class="label label-success">Accepted</span>';
				} else if (nt['is_accepted']==false) {
					nt['message'] = nt['message']+' <span class="label label-danger">Declined</span>';
				}


				nt_message.html(nt['message']);
				time.html(nt['created']);
				time.attr('title',nt['created']);

				console.log(el);

				el.appendTo($('.notifications-list'));
				el.removeClass('js-template');

			});

			$('#notificationsModal .preloader').hide();
			assign_events();
			

		},
		error: function(xhr, textStatus, errorThrown) {
			
		}
	});
}

var assign_events = function(){

	$('div.timeago').timeago();

	$('.nt-item-link').click(function(event){
		
		$('#notificationsModal').modal("hide");
		$('#ntItemModal').modal("show");

		var ntID = $(this).attr('data-nt-id');
		var itemID = $(this).attr('data-item-id');
		var ntHTML = $('#'+ntID).clone();
		var collectionName = ntHTML.attr('data-collection-name');
		$('.notification .nt-item').remove();
		$('.notification .container').prepend(ntHTML);
		$('.notification .nt-message').append(" for collection")
		$('.notification .question').html('<span>'+collectionName+'</span>');
		$('#ntItemModal .preloader').show();

		$.ajax({
			type: "GET",
			url: "/r/i_html/"+itemID+"/",  
			success: function(data) {
				$('#ntItemModal .preloader').hide();
				$('#ntItemModal .c-items').html(data);
				$('#ntItemModal .c-item .footer').hide();
				$('.sg-buttons').show();
				
			},
			error: function(xhr, textStatus, errorThrown){
				console.log(textStatus);
			}
		});

		$('.sg-accept-btn').on('click',function(){
			$('#ntItemModal .preloader').show();
			$('#ntItemModal .preloader').append("Please wait while saving.");
			$('.sg-buttons').hide();

			$.ajax({
				type: "GET",
				url: "/r/i/"+itemID+"/accepted/",
				dataType: "json",
				contentType: "json",
				success: function(data) {
					$('.notification .message').html('<h1>You accepted the suggestion</h1>');
					$('.notification .message').show();
					$('.notification .c-item').remove();
					$('#ntItemModal .preloader').hide();
					var id = $('.notification .nt-item').attr('id');
					console.log($('.notifications-list #'+id+' .nt-indicator'));
					$('.notifications-list #'+id+' .nt-indicator').remove();


					setTimeout(function(){
						$('#ntItemModal').modal('hide');
						$('#notificationsModal').modal("show");
						$('.notification .message').hide();
					},2000);

				},
				error: function(xhr, textStatus, errorThrown){
					console.log(errorThrown);
				}
			});			
		});

		$('.sg-nevermind-btn').on('click',function(){
			$('#ntItemModal .preloader').show();
			$('.sg-buttons').hide();

			$.ajax({
				type: "GET",
				url: "/r/i/"+itemID+"/nevermind/",
				dataType: "json",
				contentType: "json",
				success: function(data) {
					$('.notification .c-item').remove();
					$('#ntItemModal .preloader').hide();
					$('.notification .message').html('<h1>You declined the suggestion</h1>');
					$('.notification .message').show();
					var id = $('.notification .nt-item').attr('id');
					console.log($('.notifications-list #'+id+' .nt-indicator'));
					$('.notifications-list #'+id+' .nt-indicator').remove();

					setTimeout(function(){
						$('#ntItemModal').modal('hide');
						$('#notificationsModal').modal("show");
						$('.notification .message').hide();
					},2000);
					
				},
				error: function(xhr, textStatus, errorThrown){
					console.log(errorThrown);
				}
			});		

		});

		event.preventDefault();
		event.stopPropagation();
	});	
}
