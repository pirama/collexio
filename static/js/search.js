$(function(){
	$('.icon-search-container').addClass('active');
	$('.btn-close').addClass('active');
	//$('.top-menu').addClass('hidden');
	$('.logo').addClass('hidden');
	$('#searchClear').hide();
	$('#searchInput').focus();

	$(document).on('keyup', '#searchInput', function (event) {
		if (event.which === 13) {
			var _tmpsearchInput_val=$('#searchInput').val();
			document.location.href='?q='+encodeURIComponent(_tmpsearchInput_val)+($('.search-result-header input[name=only_my]').prop("checked")?'&only_my=true':'');
		}
	});

	$(document).on('click', '.search-nav .btn-search', function (event) {
		var _tmpsearchInput_val=$('#searchInput').val();
		document.location.href='?q='+encodeURIComponent(_tmpsearchInput_val)+($('.search-result-header input[name=only_my]').prop("checked")?'&only_my=true':'');
	});

	$(document).on('click', '.search-result-header .search-menu a', function (e) {
		var _tmpsearchInput_val=$('#searchInput').val();
		document.location.href=$(this).attr('href')+'?q='+encodeURIComponent(_tmpsearchInput_val)+($('.search-result-header input[name=only_my]').prop("checked")?'&only_my=true':'');
		e.preventDefault();
	});

	$(document).on('change', '.search-result-header input[name=only_my]', function (e) {
		var _tmpsearchInput_val=$('#searchInput').val();
		document.location.href='?q='+encodeURIComponent(_tmpsearchInput_val)+($('.search-result-header input[name=only_my]').prop("checked")?'&only_my=true':'');
	});

	$(document).on('click', '.c-items .btn-load-more', function(){
		var _tmpsearchInput_val=$('#searchInput').val();
		var _href='?q='+encodeURIComponent(_tmpsearchInput_val)+($('.search-result-header input[name=only_my]').prop("checked")?'&only_my=true':'');

		var page = $('#pagingData').data("next");
		$.ajax({
			type: "GET",
			url: _href,
			data: {
				page: page
			},
			dataType: "html",
			success: function(data) {
				$('#pagingData').remove();
				$('.items-wrapper').append(data);
				console.log($('#pagingData').length);

				var has_next = (String($('#pagingData').data("has-next")).toLowerCase()=='true'?true:false);
				if (!has_next)$('#loadMoreWrapper').remove();
			},
			error: function(jqXHR,textStatus,errorThrown) {
			},
			complete: function(){
			}
		});
	});

});