var isExtension = false;
var radius = 0;
var interval = window.setInterval(function() {
	$(".logo a").css({"-webkit-mask": "-webkit-gradient(radial, 17 17, " + radius + ", 17 17, " + (radius + 15) + ", from(rgb(0, 0, 0)), color-stop(0.5, rgba(0, 0, 0, .2)), to(rgb(0, 0, 0)))"});
	radius++;
	if (radius === 200) {
		window.clearInterval(interval);
	}
}, "fast");

$(".dropdown-menu").click(function(event) {
	event.stopPropagation();
});

$("[data-toggle='tooltip']").tooltip();
$(".add-tooltip").tooltip();

$("a").each(function() {
	var a = new RegExp("/" + window.location.host + "/");
	if (!a.test(this.href)) {
		$(this).click(function(event) {
			event.preventDefault();
			event.stopPropagation();
			window.open(this.href, "_blank");
		});
	}
});

$("#tags").tagsInput();

if ($("#c-cover").length) {
	var loader = new ImageLoader("#c-cover", {
		placeholder: ".collection-hero",
		method: "css"
  	});
}

if ($(".btn-follow").length) {
	$(".btn-follow").followBtn();
}



$(".item-video").on("click", function() {
	var w = $(this).width();
	var h = $(this).height();
	$(this).find("iframe").width(w);
	$(this).find("iframe").height(h);
	$(this).find("iframe").attr('src', function() {
        return $(this).data('src');
    });

	$(this).find(".iframe").show();
	$(this).find(".preview").hide();
});

$(window).scroll(function() {
	var y = $(window).scrollTop();
	if (y > 480) {
		$(".collection-top-nav .title").show();
		$(".collection-top-nav").addClass("fixed");
	} else {
		$(".collection-top-nav .title").hide();
		$(".collection-top-nav").removeClass("fixed");
	}
});

function scroll_to_top() {
	var windowWidth = $(window).width(),
	didScroll = false;


	var $up = $("#go-to-top");

	$up.click(function(event) {
		$("html, body").animate({
			scrollTop: "0"
		});
		event.preventDefault();
	})

	$(window).scroll(function() {
		didScroll = true;
	});

	setInterval(function() {
		if (didScroll) {
			didScroll = false;

			if ($(window).scrollTop() > 200) {
				$up.css("display", "block");
			} else {
				$up.css("display", "none");
			}
		}
	}, 250);
}
scroll_to_top();




$(document).ready(function(){
	$('.embedly-url').embedly({key: '1457678e02504979b6c7eb1170968ddd'});

	var iframes = $('.video-item-iframe iframe');

	iframes.attr('data-src', function() {
	    var src = $(this).attr('src');
	    $(this).removeAttr('src');
	    return src;
	});

	$('*').on('touchstart', function () {
        $(this).trigger('hover');
    }).on('touchend', function () {
        $(this).trigger('hover');
    });

	$(".btn-featured-collections").click(function() {
		console.log($('#featured').offset().top);
		$('body,html').animate({scrollTop: $('#featured').offset().top},'slow');
	});	

	
	if($('.messages').html()!=undefined){
		$('.messages').addClass('animated bounceInDown');
		$('.messages').show();
		setTimeout(function(){
			$('.messages').removeClass('bounceInDown');
			$('.messages').addClass('bounceOutUp');
		},3500);
	}
});

$('#create-collection-btn').on('click',function(){
	$('#signInModal h3').html("Sign in to start creating your collections!!");
    $('#signInModal').modal("show");
});


