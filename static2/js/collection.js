$(window).scroll(function() {
	var y = $(window).scrollTop();
	var loadStartingHeight = $(window).height()-375
	if (y > loadStartingHeight) {
	}
});

$('.c-list .btn-load-more').on('click',function(){
	var listType = $('#pagingData').data("list");
	var page = $('#pagingData').data("next");
	var profile = $('#pagingData').data("profile");
	$.ajax({
		type: "GET",
		url: "/c/list/"+listType+"/",
		data: {
			'p':page,
			'a':profile
		},
		dataType: "html",
		success: function(data) {
			$('#pagingData').remove();
			$('#collectionList').append(data);
			console.log($('#pagingData').length);

			if ($('#pagingData').length==0){
				$('#loadMoreWrapper').remove();
			}
		},
		error: function(jqXHR,textStatus,errorThrown) {
			$('#messageModal').modal("show");
		},
		complete: function(){
		}
	});
});

//$('#bottom-popup').modal('show');

var popupClosed = 0;

$(window).scroll(function() {
	var y = $(window).scrollTop();
	if (y > 675) {
		if (popupClosed!=1) $("#bottom-popup").slideDown();
	} else {
		$("#bottom-popup").slideUp();
	}
});

$(".popup-header > button.close").on("click",function(){
	$("#bottom-popup").slideUp();
	popupClosed = 1;
});

$('#recommendations-bar').on('click',function(){
	var recommendation_count = $('#recommendations-bar').attr('data-count');

	if ($('.recommended-items').css('display')=="block"){
		$('#recommendations-bar').html("You have "+recommendation_count+" recommendations for this collection");
		$('.recommended-items').hide();
	} else {
		$('#recommendations-bar').html("Hide recommedations");
		$('.recommended-items').show();
	}
	
});

itemLoadMoreBtn = $('.c-items .btn-load-more');
itemLoadMoreBtn.on('click',function(){
	var page = $('#pagingData').attr('data-next');
	var collection = $('#pagingData').attr('data-collection');

	$.ajax({
		type: "GET",
		url: "/r/c/"+collection+"/items/",
		data: {
			'p':page,	
		},
		dataType: "json",
		success: function(data) {
			$('#itemsContainer').append(JSON.parse(data['html']));

			$('.edit-item').off();
			$('.edit-item').on('click',function(event){
				editItemform(event,this);
			});

			$(".delete-item").off();
			$(".delete-item").on("click",function(event){
				deleteItem(event,this);
			});
			
			$('.embedly-url').embedly({key: '1457678e02504979b6c7eb1170968ddd'});

			$(".item-video").on("click", function() {
				var w = $(this).width();
				var h = $(this).height();
				$(this).find("iframe").width(w);
				$(this).find("iframe").height(h);
				$(this).find("iframe").attr('src', function() {
			        return $(this).data('src');
			    });

				$(this).find(".iframe").show();
				$(this).find(".preview").hide();
			});
			
			$('#pagingData').attr('data-next', data['p']['next_page_number']);
			$('#pagingData').attr('data-has-next', data['p']['has_next']);

			if ($('#pagingData').attr('data-has-next')=="false"){
				$('#loadMoreWrapper').remove();
			}
			/*
			$('#pagingData').remove();
			$('#collectionList').append(data);
			console.log($('#pagingData').length);			
			*/

			applyLayout();
		},
		error: function(jqXHR,textStatus,errorThrown) {
			//$('#messageModal').modal("show");
			console.log(textStatus);
		},
		complete: function(){
		}
	});
});

/*
Pinboard plugin
https://github.com/GBKS/Wookmark-jQuery/blob/master/example-endless-scroll/index.html
*/

var container = $('#itemsContainer');
var handler = $('.c-item');

var options = {
	container: container,
	itemWidth: 320,
	flexibleWidth: true,
	autoResize:true,
	offset: 32,
}

$(document).ready(function(){
	applyLayout();

});

function applyLayout() {

	// Destroy the old handler
	if (handler.wookmarkInstance) {
		handler.wookmarkInstance.clear();
	}
	// Create a new layout handler.
	handler = $('.c-item');
	handler.wookmark(options);

    container.imagesLoaded(function() {
    	applyLayout();
    });
}


/*
$(window).scroll(function() {
	var y = $(window).scrollTop();
	if (y > 675) {
		ga('send', 'event',{
		  'eventCategory': 'page-view',
		  'eventAction': 'scroll',
		  'eventLabel': 'collection-page-scroll',
		});
	}
});
*/