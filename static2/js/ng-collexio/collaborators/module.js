var app = angular.module('collaborators',[]);

app.factory('Collaborators', function($resource) {
	return $resource('/c/:cid/collaborators/:id');
});

stream.directive('CollaboratorsDirective',function(){
    return {
        scope: {
            postId: '=post_id',
            is_event: '=is_event',
            profileId: '=profile_id'
        },
        templateUrl: '/static/js/libs/wng/stream/tpl/mediaModal.html?aa',
        link: function(scope,element,attrs) {
            Session.get().then(function(session) {

                var postList = PostCollection.getCollection("home", session.profile.id);
                commentInput = angular.element('#commentText');

                postList.fetchPostData(attrs['postId']).then(function(post) {
                    post = Post.getPostByData(post);
                    scope.post = post;
                });

                scope.onSendCommentClick = function() {
                    scope.post.sendComment(commentInput.val());
                }

                scope.onCloseBtnClick = function(){
                    $(element).remove();
                }

                scope.onNextBtnClick = function(){
                    postList.fetchPostData(scope.post.id,"next",true,attrs['profileId']).
                        then(function(post) {
                            post = Post.getPostByData(post);
                            scope.post = post;
                        });
                }

                scope.onPrevBtnClick = function(){
                    postList.fetchPostData(scope.post.id,"prev",true,attrs['profileId']).
                        then(function(post) {
                            post = Post.getPostByData(post);
                            scope.post = post;
                        });
                }
            });
        }
    }
});